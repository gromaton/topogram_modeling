unit CompMath;

interface
type
  Complex=record
    r,i: extended;
  end;

const
  CNul: complex = (r:0; i:0);
  CRe1: complex = (r:1; i:0);
  CIm1: complex = (r:0; i:1);

function cexp(const X: complex): complex;
function ccos(const X: complex): complex;
function csin(const X: complex): complex;
function csum(const X,Y: complex): complex;    {+}
function cdif(const X,Y: complex): complex;    {-}
function cdiv(const X,Y: complex): complex;    {/}
function cmul(const X,Y: complex): complex;    {*}

function ctan(const X: complex): complex;

function ReToComp(const X: extended): complex;
function CAbs(const X: complex): extended;

function MinV(const X,Y: integer): integer;
function MaxV(const X,Y: integer): integer;

{function CNul(): complex;
function CRe1(): complex;
 }

implementation
Uses Math;

function cexp(const X: complex): complex;
begin
  cexp.r:=exp(X.r)*cos(X.i);
  cexp.i:=exp(X.r)*sin(X.i);
end;

function ccos(const X: complex): complex;
begin
  ccos.r:=cos(X.r)*cosh(X.i);
  ccos.i:=-sin(X.r)*sinh(X.i);
end;

function csin(const X: complex): complex;
begin
  csin.r:=sin(X.r)*cosh(X.i);
  csin.i:=cos(X.r)*sinh(X.i);
end;

function csum(const X,Y: complex): complex;
begin
  csum.r:=X.r+Y.r;
  csum.i:=X.i+Y.i;
end;

function cdif(const X,Y: complex): complex;
begin
  cdif.r:=X.r-Y.r;
  cdif.i:=X.i-Y.i;
end;

function cdiv(const X,Y: complex): complex;
begin
  cdiv.r:=(X.r*Y.r+X.i*Y.i)/(Y.r*Y.r+Y.i*Y.i);
  cdiv.i:=(X.i*Y.r-X.r*Y.i)/(Y.r*Y.r+Y.i*Y.i);
end;

function cmul(const X,Y: complex): complex;
begin
  cmul.r:=X.r*Y.r-X.i*Y.i;
  cmul.i:=X.r*Y.i+X.i*Y.r;
end;

function ReToComp(const X: extended): complex;
begin
  ReToComp.r:=X;
  ReToComp.i:=0;
end;

function CAbs(const X: complex): extended;
begin
  CAbs:=Hypot(X.r,X.i);
end;

function MaxV(const X,Y: integer): integer;
begin
  if X<Y then
    MaxV:=Y
  else
    MaxV:=X;
end;

function MinV(const X,Y: integer): integer;
begin
  if X<Y then
    MinV:=X
  else
    MinV:=Y;
end;

function ctan(const X: complex): complex;
begin
  ctan:=cdiv(csin(X),ccos(X));
end;

end.
