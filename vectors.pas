unit vectors;

interface

uses Math, Windows, SysUtils;

type
  Vector = Array [1..4] of Extended; //������� ���� Vector-������������
  Matrix = Array [1..3,1..3] of Extended; //������� ���� Matrix
  Direct = Array [1..3] of SmallInt; //������ ����� ��������

function GetVector(x,y,z: extended): Vector; //���������� ������ � ������������ x,y,z � ������������� 4-�� ������� ������� ��� ��� ������
function GetRotateMatrix(Axis: Char; Fi: extended): Matrix; overload;//���������� ������� �������� ������ Axis (x,y ��� z) �� Fi ��������
function Mdl(V: Vector): Double; //��������� ������ �������
function NormVec(V: Vector): Vector; //������������ ������� �� �������
function OrtVec(A: Vector): Vector; {���������� ������ ����������������� �������}
function VecxVec(A,B: Vector): Vector; {��������� ������������}
function Prot(x: Extended): Extended; //����������� ����� � ��������
function AngleBtwnVectors(A,B: Vector): Extended; //���� ����� ���������
function IsEqualVecs(A,B: Vector): Boolean; //�������� �������� �� ���������
function IsNullVector(V: Vector): boolean;  //���� ������ ������� ����� ����, ���������� true
function MatxVec(M: Matrix; V: Vector): Vector; //��������� ������� �� ������
function Determinant(M: Matrix): Extended; //������������ �������
function InverseMatrix(M: Matrix): Matrix; //�������� �������
function SLAU(A: Matrix; B: Vector): Vector; //������� ����, ���������� ������-�������
function MatxMat(A,B: Matrix): Matrix; //������������ ���� ������
function RotateVector(Axis, Vec: Vector; Fi: Extended): Vector; //������� ������� Vec ������ ��� Axis �� ���� Fi
function SumVecs(A,B: Vector): Vector; //������������ ���� ��������
function DifVecs(A,B: Vector): Vector; //�������� ���� ��������
function ScalarMult(A,B: Vector): extended; //��������� ������������
function VecToStr(V: Vector): string; //����������� ������ � ������, ����������� - ������
function GetReflectVector(L,N: Vector): Vector; //����������� ���������� ������, L-�������� ���,N-������� � ����������� � ����� �������
function GetRotateMatrix(Axis: Vector; Fi: Extended): Matrix; overload;
function GetSingleMatrix(): Matrix;

implementation

function GetVector(x,y,z: extended): Vector;
begin
  Result[1]:=x;
  Result[2]:=y;
  Result[3]:=z;
  Result[4]:=sqrt(sqr(x)+sqr(y)+sqr(z));
end;

function GetRotateMatrix(Axis: Char; Fi: extended): Matrix;
begin
  if UpCase(Axis)='Z' then begin
    Result[1][1]:=cos(Fi);  Result[1][2]:=-sin(Fi); Result[1][3]:=0;
    Result[2][1]:=sin(Fi);  Result[2][2]:=cos(Fi);  Result[2][3]:=0;
    Result[3][1]:=0;        Result[3][2]:=0;        Result[3][3]:=1;
  end
  else if UpCase(Axis)='Y' then begin
    Result[1][1]:=cos(Fi);  Result[1][2]:=0; Result[1][3]:=sin(Fi);
    Result[2][1]:=0;        Result[2][2]:=1; Result[2][3]:=0;
    Result[3][1]:=-sin(Fi); Result[3][2]:=0; Result[3][3]:=cos(Fi);
  end
  else if UpCase(Axis)='X' then begin
    Result[1][1]:=1;  Result[1][2]:=0;       Result[1][3]:=0;
    Result[2][1]:=0;  Result[2][2]:=cos(Fi); Result[2][3]:=-sin(Fi);
    Result[3][1]:=0;  Result[3][2]:=sin(Fi); Result[3][3]:=cos(Fi);
  end
  else begin
    MessageBox(0,'Wrong Axis','Error',MB_ICONERROR);
    Halt;
  end;
end;

function Mdl(V: Vector): Double;
begin
 Result:=sqrt(sqr(V[1])+sqr(V[2])+sqr(V[3]));
end;

function NormVec(V: Vector): Vector;
begin
  V[4]:=sqrt(sqr(V[1])+sqr(V[2])+sqr(V[3]));
  V[1]:=V[1]/V[4];
  V[2]:=V[2]/V[4];
  V[3]:=V[3]/V[4];
  Result:=V;
end;


function OrtVec(A: Vector): Vector;
var i,S0,S0pos: Byte;
    S: Extended;
begin
 S:=0; S0:=0; S0pos:=0;
 for i:=1 to 3 do S:=S+abs(A[i]);
 for i:=1 to 3 do if A[i]=0 then begin S0:=S0+1;S0pos:=i;end;
 if S0=0 then begin Result[1]:=1; Result[2]:=1; Result[3]:=-(A[1]+A[2])/A[3]; end;
 if S0=1 then
 case S0pos of
   1: begin  Result[1]:=1; Result[2]:=1; Result[3]:=-A[2]/A[3]; end;
   2: begin  Result[1]:=1; Result[2]:=1; Result[3]:=-A[1]/A[3]; end;
   3: begin  Result[2]:=1; Result[3]:=1; Result[1]:=-A[2]/A[1]; end;
 end;
 if S0=2 then begin
        if A[1]<>0 then begin Result[1]:=0;Result[2]:=1;Result[3]:=0; end;
        if A[2]<>0 then begin Result[1]:=0;Result[2]:=0;Result[3]:=1; end;
        if A[3]<>0 then begin Result[1]:=1;Result[2]:=0;Result[3]:=0; end;
 end;
end;

function VecxVec(A,B: Vector): Vector;
begin
 Result[1]:=B[3]*A[2]-B[2]*A[3];
 Result[2]:=B[1]*A[3]-B[3]*A[1];
 Result[3]:=B[2]*A[1]-B[1]*A[2];
end;

function Prot(x: Extended): Extended;
begin
 Result:=0-x;
end;

function AngleBtwnVectors(A,B: Vector): Extended;
var modA,modB,Fi,Arg: Extended;
begin
 modA:=sqrt(sqr(A[1])+sqr(A[2])+sqr(A[3]));
 modB:=sqrt(sqr(B[1])+sqr(B[2])+sqr(B[3]));
 Arg:=(A[1]*B[1]+A[2]*B[2]+A[3]*B[3])/(modA*modB);
 if Arg>1 then Arg:=1; if Arg<-1 then Arg:=-1;
 //Fi:=ArcCos(Arg);   //todo1
 //Result:=Fi*180/Pi;
 Result:=ArcCos(Arg);
end;

function IsEqualVecs(A,B: Vector): Boolean;
begin
  if (A[1]=B[1])and(A[2]=B[2])and(A[3]=B[3]) then Result:=true else Result:=false;
end;

function IsNullVector(V: Vector): boolean;
begin
  if (V[1]=0)and(V[2]=0)and(V[3]=0) then Result:=true else Result:=false;
end;

function MatxVec(M: Matrix; V: Vector): Vector;
var
  i: byte;
begin
 Result[1]:=M[1][1]*V[1]+M[1][2]*V[2]+M[1][3]*V[3];
 Result[2]:=M[2][1]*V[1]+M[2][2]*V[2]+M[2][3]*V[3];
 Result[3]:=M[3][1]*V[1]+M[3][2]*V[2]+M[3][3]*V[3];
 //for i:=1 to 3 do
   //if (Result[i]<0.0000001)and(Result[i]>-0.0000001) then Result[i]:=0;
end;

function Determinant(M: Matrix): Extended;
begin
  Result:=M[1][1]*(M[2][2]*M[3][3]-M[2][3]*M[3][2])
          -M[1][2]*(M[2][1]*M[3][3]-M[2][3]*M[3][1])
          +M[1][3]*(M[2][1]*M[3][2]-M[2][2]*M[3][1]);
end;

function InverseMatrix(M: Matrix): Matrix;
var
  det: Extended;
begin
  det:=Determinant(M);
  if det=0 then
  begin
    MessageBox(0,'det = 0 in InverseMatrix','Error',MB_ICONERROR);
    Halt;
  end;
  //������ ������. ������������ �������������� ���������� I-�� ������� ������� M
  Result[1][1]:=(M[2][2]*M[3][3]-M[2][3]*M[3][2])/det;
  Result[1][2]:=-(M[1][2]*M[3][3]-M[1][3]*M[3][2])/det;
  Result[1][3]:=(M[1][2]*M[2][3]-M[1][3]*M[2][2])/det;
  //������ ������. �������������� ���������� II-��� �������
  Result[2][1]:=-(M[2][1]*M[3][3]-M[2][3]*M[3][1])/det;
  Result[2][2]:=(M[1][1]*M[3][3]-M[1][3]*M[3][1])/det;
  Result[2][3]:=-(M[1][1]*M[2][3]-M[1][3]*M[2][1])/det;
  //������ ������. �������������� ���������� III-��� �������
  Result[3][1]:=(M[2][1]*M[3][2]-M[2][2]*M[3][1])/det;
  Result[3][2]:=-(M[1][1]*M[3][2]-M[1][2]*M[3][1])/det;
  Result[3][3]:=(M[1][1]*M[2][2]-M[1][2]*M[2][1])/det;
end;

function SLAU(A: Matrix; B: Vector): Vector;
var
  ObrA: Matrix; //�������� �������
begin
  ObrA:=InverseMatrix(A);
  result:=MatxVec(ObrA,B);
end;

function MatxMat(A,B: Matrix): Matrix;
var
  i,j,k: Byte;
begin
  for k:=1 to 3 do
    for i:=1 to 3 do
    begin
    Result[i][k]:=0;
      for j:=1 to 3 do
        Result[i][k]:=Result[i][k]+A[i][j]*B[j][k];
    end;
end;

function RotateVector(Axis, Vec: Vector; Fi: Extended): Vector;
var
  MPovorota,MRotX,MRotX1,MRotY,MRotY1: Matrix;
  T: Matrix;
  i,j: Word;
  d,Psi: Extended;
begin
  NormVec(Axis);
//������ ���� � ������� �������� ������������ ��� X
 if (Axis[1]=0)and(Axis[2]=0) then
 begin
  MRotX[1][1]:=1; MRotX[1][2]:=0; MRotX[1][3]:=0;
  MRotX[2][1]:=0; MRotX[2][2]:=1; MRotX[2][3]:=0;
  MRotX[3][1]:=0; MRotX[3][2]:=0; MRotX[3][3]:=1;

  MRotY[1][1]:=1; MRotY[1][2]:=0; MRotY[1][3]:=0;
  MRotY[2][1]:=0; MRotY[2][2]:=1; MRotY[2][3]:=0;
  MRotY[3][1]:=0; MRotY[3][2]:=0; MRotY[3][3]:=1;
 end
 else
 begin
  d:=sqrt(sqr(Axis[2])+sqr(Axis[3]));
  if d<>0 then
  begin
    MRotX[1][1]:=1; MRotX[1][2]:=0;          MRotX[1][3]:=0;
    MRotX[2][1]:=0; MRotX[2][2]:=Axis[3]/d;  MRotX[2][3]:=Axis[2]/d;
    MRotX[3][1]:=0; MRotX[3][2]:=-Axis[2]/d; MRotX[3][3]:=Axis[3]/d;
  end
  else
  begin
    MRotX[1][1]:=1; MRotX[1][2]:=0; MRotX[1][3]:=0;
    MRotX[2][1]:=0; MRotX[2][2]:=1; MRotX[2][3]:=0;
    MRotX[3][1]:=0; MRotX[3][2]:=0; MRotX[3][3]:=1;
  end;
//����� ������� �������� ������������ ��� Z

//������ ���� � ������� �������� ������������ ��� Y
  //Psi:=ArcCos(Axis[3]);
  MRotY[1][1]:=d;        MRotY[1][2]:=0; MRotY[1][3]:=Axis[1];
  MRotY[2][1]:=0;        MRotY[2][2]:=1; MRotY[2][3]:=0;
  MRotY[3][1]:=-Axis[1]; MRotY[3][2]:=0; MRotY[3][3]:=d;
//����� ������� �������� ������������ ��� Y
 end;

//������ ������� �������� �� ����������� ���� ������������ ��� Z
  //Psi:=Fi*Pi/180;    //todo1

  MPovorota[1][1]:=cos(Fi); MPovorota[1][2]:=-sin(Fi); MPovorota[1][3]:=0;
  MPovorota[2][1]:=sin(Fi);MPovorota[2][2]:=cos(Fi); MPovorota[2][3]:=0;
  MPovorota[3][1]:=0;        MPovorota[3][2]:=0;        MPovorota[3][3]:=1;
//����� ������� �������� �� ������������ ����

//������ ������� ��������������
  T:=MatxMat(MRotX,MRotY);
  T:=MatxMat(T,MPovorota);
  T:=MatxMat(T,InverseMatrix(MRotY));
  T:=MatxMat(T,InverseMatrix(MRotX));
//����� ������� ������� ��������������
//�������
  Result:=MatxVec(T,Vec);//�������� � ������ ��������
  //NormVec(Result[i]);
//����� ��������
end;

function SumVecs(A,B: Vector): Vector;
begin
  Result:=GetVector(A[1]+B[1],A[2]+B[2],A[3]+B[3]);
end;

function DifVecs(A,B: Vector): Vector;
begin
  Result:=GetVector(B[1]-A[1],B[2]-A[2],B[3]-A[3]);
end;


function ScalarMult(A,B: Vector): extended;
begin
  Result:=A[1]*B[1]+A[2]*B[2]+A[3]*B[3];
end;

function VecToStr(V: Vector): string;
begin
  Result:=FloatToStr(V[1])+' '+FloatToStr(V[2])+' '+FloatToStr(V[3]);
end;

function GetReflectVector(L,N: Vector): Vector;
var
  V: Vector;
  M: Matrix;
begin
  if ScalarMult(N,L)=0 then begin //���� �������� ��� ��������������� ��������� �������, �� ���������� ����� ��� �� ����� ���
    Result:=L;
    exit;
  end;
  V:=VecxVec(N,L);
  if N[1]<>0 then begin
    M[1][1]:=N[1];  M[1][2]:=N[2];  M[1][3]:=N[3];
    M[2][1]:=-N[3]; M[2][2]:=0;     M[2][3]:=N[1];
    M[3][1]:=N[2];  M[3][2]:=-N[1]; M[3][3]:=0;
    V[1]:=ScalarMult(N,L);
  end
  else if N[2]<>0 then begin
    M[1][1]:=0;    M[1][2]:=N[3];  M[1][3]:=-N[2];
    M[2][1]:=N[1]; M[2][2]:=N[2];  M[2][3]:=N[3];
    M[3][1]:=N[2]; M[3][2]:=-N[1]; M[3][3]:=0;
    V[2]:=ScalarMult(N,L);
  end
  else begin
    M[1][1]:=0;     M[1][2]:=N[3]; M[1][3]:=-N[2];
    M[2][1]:=-N[3]; M[2][2]:=0;    M[2][3]:=N[1];
    M[3][1]:=N[1];  M[3][2]:=N[2]; M[3][3]:=N[3];
    V[3]:=ScalarMult(N,L);
  end;
  Result:=SLAU(M,V);
end;

function GetSingleMatrix(): Matrix;
begin
  Result[1,1]:=1; Result[1,2]:=0; Result[1,3]:=0;
  Result[2,1]:=0; Result[2,2]:=1; Result[2,3]:=0;
  Result[3,1]:=0; Result[3,2]:=0; Result[3,3]:=1;
end;

function GetRotateMatrix(Axis: Vector; Fi: Extended): Matrix;
var
  MPovorota,MRotX,MRotY: Matrix;
  d: Extended;
  i,j: byte;
begin
  Axis:=NormVec(Axis);
//?????? ?????? ???????? ???????????? ??? X ? Y
  if (Axis[1]=0)and(Axis[2]=0) then begin
    MRotX:=GetSingleMatrix;
    MRotY:=GetSingleMatrix;
  end
  else begin
    d:=sqrt(sqr(Axis[2])+sqr(Axis[3]));
    if d<>0 then begin
      MRotX[1,1]:=1; MRotX[1,2]:=0;          MRotX[1,3]:=0;
      MRotX[2,1]:=0; MRotX[2,2]:=Axis[3]/d;  MRotX[2,3]:=Axis[2]/d;
      MRotX[3,1]:=0; MRotX[3,2]:=-Axis[2]/d; MRotX[3,3]:=Axis[3]/d;
    end
    else begin
      MRotX:=GetSingleMatrix;
    end;

    MRotY[1,1]:=d;        MRotY[1,2]:=0; MRotY[1,3]:=Axis[1];
    MRotY[2,1]:=0;        MRotY[2,2]:=1; MRotY[2,3]:=0;
    MRotY[3,1]:=-Axis[1]; MRotY[3,2]:=0; MRotY[3,3]:=d;
  end;

  MPovorota[1,1]:=cos(Fi); MPovorota[1,2]:=-sin(Fi); MPovorota[1,3]:=0;
  MPovorota[2,1]:=sin(Fi); MPovorota[2,2]:=cos(Fi);  MPovorota[2,3]:=0;
  MPovorota[3,1]:=0;       MPovorota[3,2]:=0;        MPovorota[3,3]:=1;



  Result:=MatxMat(MRotX,MRotY);
  Result:=MatxMat(Result,MPovorota);
  Result:=MatxMat(Result,InverseMatrix(MRotY));
  Result:=MatxMat(Result,InverseMatrix(MRotX));
  for i:=1 to 3 do
    for j:=1 to 3 do
      if (Result[i,j]>-1e-15) and (Result[i,j]<1e-15) then Result[i,j]:=0;
end;

end.
