unit structure;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, ComCtrls, StrUtils, Math, vectors, CompMath, mxarrays, mxcommon;

type
  TMesh = array of array[1..4] of extended;
  TXRayTubeAnod = (taAgKalfa1,taAgKalfa2,taAgKbeta1,taAgKbeta2,taCoKalfa1,taCoKalfa2,taCoKbeta1,taCrKalfa1,taCrKalfa2,taCrKbeta1,taCuKalfa1,taCuKalfa2,taCuKbeta1);
  TWinPosition = (wpTop, wpMiddle, wpBottom);
  
  TDiff = packed record
    dZdX: extended;
    dZdY: extended;
  end;

  //All angles are set and stored in radians
  TLayer = class
  private
    Mesh: TMesh; //4xn matrix - is a set of points that make surface as a mesh, culumns - x,y,z coordinates and h thickness in this point, N total mesh points number.
    LattParam: extended; //�������� ������� ����
    EpsZ: extended; //���������������� ����������
    Relax: extended; //������� ���������� � % (������� ������ ��� 0 � ��� 100%)
    vDirAxisY: Vector; //�������������������� ����������� ��� Y ����
    function GetFi0(h,k,l: extended): extended; //������ ���� ���������
    function Dhkl(h,k,l: extended): extended;  //������ ��������������� ����������
    function BraggAngle(h,k,l,Lambda: extended; Razmernost: string = 'Rad'): extended; //��������� ���� ������
  public
    DPmmX,DpmmY: integer; //���������� (���������� ����� �� 1 ��) �� ���� X � Y �������������
    vSurfaceNormals: array of Vector; //������� � ����������� ���� � ������ ����� �����
    constructor Create;
    //destructor Destroy; override;

    function ImportReliefMatrix(ImportFile: string): integer; //������ ����� (�� ��������)
  end;

  TStructure = class
  private
    NLayers: byte; //���������� �����
    FNumberOfNodes: integer; //���������� ����� �����
    function Diff(LayerN,pntN: integer): TDiff; //������ ������� ����������� � ���� pntN ���� LayerN
  public
    SizeX,SizeY: extended; //������� ��������� � ��
    Layer: array of TLayer; //������ �����, ������������ ���������
    constructor Create(WidthX,WidthY:extended;NLayer: byte = 3); //������� ��������� � ��������� ��������� � ����������� �����
    destructor Destroy; override;
    procedure SetLayerParams(LayerN: Byte; LatticeParametr,EpsilonZ: extended; vDirY: Vector; Relaxation: extended = 0); //������������� ��������� ����
    function SetCylLayerProfil(LayerN: byte; R,h: extended; DotsPerMMX: integer = 10; DotsPerMMY: integer = 10; CylAxisAngle: extended = 0): integer; //������ ��������������� ������� �����������. ��� �������� ��������� �� ����������� [010] �� ���� CylAxisAngle
    function GetNumNodes(): integer; //���������� ���������� ����� �����
    function ExportReliefMatrix(LayerN: integer; ExportFile: string): integer; //������� �����
    function SetThicknessGradientY(LayerN: byte; HStart,HEnd: extended): integer; //������� �������� ������� � ����������� ��� Y
    function SetThicknessGradientX(LayerN: byte; HStart,HEnd: extended): integer; //������� �������� ������� � ����������� ��� X
    function GetNormals(LayerN: byte): integer; //����������� ������ �������� � ���������� ��� � vSurfaceNormals ��� ���� LayerN
    //������ ������������� Value � �������� ������ ��� Y, � ����������� ��� x ��� y, ������� � From ���������� Upto ��
    function SetLayerMisorientation(LayerN: byte; Value,From,Upto: extended; Dir: Char{x or y}): integer;
  end;

  TXRay = class
  private
    Lambda: extended; //����� ����� ���������
    LWinToSource: extended; //���������� �� ��������� �� ��������� ����
    VSpread, HSpread: extended; //������������ � �������������� ������������ �����
    VSourseSize,HSourceSize: extended; //������� ���������
    VWinSize,HWinSize: extended; //������������ � �������������� ������ ��������� ����
    function LabmdaOf(Anod: TXRayTubeAnod): extended; //���������� ����� �����, � ����������� �� ���������� ������
  public

    constructor Create();
    procedure SetLambda(Anod: TXRayTubeAnod); //������������� �������� Lambda � ������������ � ����� ������
    procedure SetSourceParams(HSize,VSize,DistanceToWin: extended); //������������� ��������� ���������
    procedure SetWinParams(HSize,VSize: extended; Position: TWinPosition = wpMiddle); //������������� ��������� ����
  end;

  TXRayTopo = class
  private
    Reflex: Vector;
    LWinToStruct: extended; //���������� �� ��������� ���� �� ������ ��������� (0,0)
    Teta0: extended; //���������� ���� ������
    dTeta0: extended; //���������� �� ������� ���� ������ � ���.���., ��� ��������� �������
    Azimut0: extended; //���� ������� � ������ ������
    vReflexDir: Vector; //������� � ��������� ���������
    vOsZ: Vector;
    Q,G0,Gh, b_: array of extended; //���� ���� � ������������ ���������� ��� ���� ����� ���������
    Psi01,Psi011,Psih1,Psih11: extended; //����������� �������
    function Diff(LayerN,pntN: integer): TDiff; //���������� ������� ����������� � �����
    function GetRayDir(LayerN,pntN: integer): Vector; //�������� ������ ��������� ���� ������������� � ����� pntN
  public
    //Fi0: extended; //���� ��������� ��������� ���������
    XRay: TXRay; //�����
    Structure: TStructure; //���������
    FallAngles: array of Extended; //������ ����� ������� ��� ������ �����
    Intensity: array of Extended; //������ ������������� ������ �����
    ReflectedVecs: array of Vector; //������� ��������� � ������ ����� �����
    PhotoFilm: TMesh; //����������, �������� ���������� ����� � �� �������������. ��� ����� ���������� ����� � ����� ���������

    Gamma0: extended; //���� ����� � ������ ������
    constructor Create;
    destructor Destroy; override;
    function GetTeta0(LayerN: byte): extended; //��������� ���� ������ ��� ���� LayerN
    function GetFi0(LayerN: byte): extended; //�������� ���� ��������� �������� ��� ��������� ���� LayerN
    procedure SetLWinToStruct(L: extended);
    function AddStructure(SizeX,SizeY: extended; NLayers: byte = 3): integer; //���������� ������� � ���������
    function DeleteStructure(): integer; //�������� ������� �� ���������
    function AddXRay(): integer; //���������� �����
    function DeleteXRay(): integer; //�������� �����
    function SetCenterPoint(x,y: extended): integer; //��������� ������� � ����� ������� ������������ ����
    procedure SetLayerParams(LayerN: Byte; LatticeParametr,EpsilonZ: extended; vDirY: Vector; Relaxation: extended = 0); //������������� ��������� ����
    function SetCylLayerProfil(LayerN: byte; R,h: extended; DotsPerMMX: integer = 10; DotsPerMMY: integer = 10; CylAxisAngle: extended = 0): integer;
    function SetThicknessGradientY(LayerN: byte; HStart,HEnd: extended): integer; //������� �������� ������� � ����������� ��� Y
    function SetThicknessGradientX(LayerN: byte; HStart,HEnd: extended): integer;
    function SetLayerMisorientation(LayerN: byte; Value,From,Upto: extended; Dir: Char{x or y}): integer;
    function SetStructureOrientation(h,k,l, Azimut, Gamma: extended; dTeta: extended = 0): integer; //�������� ���������� �������
    function GetFallAngle(LayerN,pntN: integer): extended; //������� ���� ������� ��� ����� ���� � ������
    function GetIntensity(): integer; //������ �������������� ��� ������ �����
    function GetIntensityTT(): integer; //������ ������������� �� ������ ������-������
    function GetX_Takagi(LayerN: byte; pnt: integer; X0: complex; Teta: extended): complex;
    function GetIntensityTT2(): integer;
    function GetReflectedVectors(): integer; //������ �������� ��������� ��� ������ ����� �����
    //������������� ��������������� ����� �� ����������. ������ �������� ���������� Ax+By+Cz+D=0, ��� ������ (A,B,C)=vNormal
    function GetPhotoImage(vNormal: Vector; D: extended; var Photo: TBitmap): integer;
    function GetKDO(t: extended): integer; //���������� ��� ��� ������� ��������������� ���� t, ��� ��� ����������� ��������������
    function GetIinNode(Node: integer; K0: Vector): extended; //���������� ������������ � �����. Node - ����� ���� �����, K0 - �������� �����
  end;

implementation

//TLayer
  constructor TLayer.Create;
  begin
    inherited Create;
  end;

  function TLayer.GetFi0(h,k,l: extended): extended;
  var
    d,dz,dxy,fi01: extended;
  begin
    fi01:=Arccos(l/sqrt(h*h+k*k+l*l));
    if (EpsZ<>0)and(fi01<>0) then begin //� ������ ���������������� ����������
      d:=LattParam/sqrt(h*h+k*k+l*l);
      dz:=d/cos(fi01);
      dxy:=d/sin(fi01);
      fi01:=Arctan(dz*(1+EpsZ)/dxy);
    end;
    Result:=fi01;
  end;

  function TLayer.Dhkl(h,k,l: extended): extended;
  var
    d,dz,dxy,fi01: extended;
  begin
    if LattParam=0 then
    begin
      MessageBox(0,'Parametr "LattParam" is not set. Application aborted','Error',MB_ICONERROR);
      Application.Terminate;
    end;
    d:=LattParam/sqrt(h*h+k*k+l*l);
    if Relax = 0 then begin
      if EpsZ<>0 then begin //� ������ ���������������� ����������
        if GetFi0(h,k,l)<>0 then begin
          fi01:=Arccos(l/sqrt(h*h+k*k+l*l));
          dz:=d/cos(fi01);
          dxy:=d/sin(fi01);
          d:=dxy*sin(Arctan(dz*(1+EpsZ)/dxy));
        end
        else begin
          d:=d*(1+EpsZ);
        end;
      end;
    end
    else begin
      d:=d*(1+EpsZ);
    end;
    Result:=d;
  end;

  function TLayer.BraggAngle(h,k,l,Lambda: extended; Razmernost: string = 'Rad'): extended;
  var
    Teta,d: extended;
  begin
    d:=Dhkl(h,k,l);
    if lambda<2*d then
      Teta:=arcsin(Lambda/(2*d))
    else
      Teta:=Pi/2;
    if Razmernost='Grad' then Teta:=Teta*180/Pi
    else if Razmernost='Sec' then Teta:=Teta*180*3600/Pi;
    result:=Teta;
  end;

  function TLayer.ImportReliefMatrix(ImportFile: string): integer;
  var
    i,j: integer;
  begin

  end;

//TStructure
  constructor TStructure.Create(WidthX,WidthY:extended;NLayer: byte = 3);
  var
    i: integer;
  begin
    inherited Create;
    SizeX:=WidthX;
    SizeY:=WidthY;
    NLayers:=NLayer;
    SetLength(Layer,Nlayer+1);
    for i:=1 to NLayer do
      Layer[i]:=TLayer.Create;
  end;

  destructor TStructure.Destroy;
  var
    i: byte;
  begin
    if NLayers<>0 then begin
        for i:=1 to NLayers do begin
            if Assigned(Layer[i]) then begin
                Layer[i].Free;
            end;
        end;
    end;
    inherited Destroy;
  end;

  function TStructure.ExportReliefMatrix(LayerN: integer; ExportFile: string): integer;
  var
    i: integer;
    F: TextFile;
    str: string;
    DCchanged: boolean;
  begin
    AssignFile(F,ExportFile);
    DCchanged:=false;
    try
      Rewrite(F);
      if DecimalSeparator=',' then
      begin
        DecimalSeparator:='.';
        DCchanged:=true;
      end;
      for i:=0 to High(Layer[LayerN].Mesh) do
      begin
        str:=FloatToStr(Layer[LayerN].Mesh[i][1])+', '+FloatToStr(Layer[LayerN].Mesh[i][2])+', '+FloatToStr(Layer[LayerN].Mesh[i][3]);
        Writeln(F,str);
      end;
      CloseFile(F);
    finally
      if DCchanged then DecimalSeparator:=',';
    end;
    result:=0;
  end;

  procedure TStructure.SetLayerParams(LayerN: Byte; LatticeParametr,EpsilonZ: extended; vDirY: Vector; Relaxation: extended = 0);
  begin
    Layer[LayerN].LattParam:=LatticeParametr;
    Layer[LayerN].EpsZ:=EpsilonZ;
    Layer[LayerN].Relax:=Relaxation;
    if IsNullVector(vDirY) then begin
      Layer[LayerN].vDirAxisY:=GetVector(0,1,0);
    end
    else begin
      Layer[LayerN].vDirAxisY:=vDirY;
    end;
  end;

  function TStructure.GetNumNodes(): integer;
  begin
      if High(Layer[1].Mesh)<>0 then Result:=High(Layer[1].Mesh)
      else if High(Layer[2].Mesh)<>0 then Result:=High(Layer[2].Mesh)
      else if High(Layer[3].Mesh)<>0 then Result:=High(Layer[3].Mesh)
      else begin
        Result:=0;
      end;
  end;

  function TStructure.SetCylLayerProfil(LayerN: byte; R,h: extended; DotsPerMMX: integer = 10; DotsPerMMY: integer = 10; CylAxisAngle: extended = 0): integer;
  var
    i,ResX,ResY,Npnts: integer;
    hx,hy,x,y,x1,y1,sx,N,FiOs: extended;
    mRotZ: Matrix;
    vNode: Vector;
    Nmax: extended;
  begin
        //Nmax:=MinExtended;
    {if (SizeX>(2*R)) and (R<>0) then
    begin
      MessageBox(0,'2*R must be larger then SizeX','Error',MB_ICONERROR);
      Application.Terminate;
    end;  }
    Layer[LayerN].DPmmX:=DotsPerMMX;
    Layer[LayerN].DpmmY:=DotsPerMMY;
    ResX:=Round(SizeX*DotsPerMMX); ResY:=Round(SizeY*DotsPerMMY);
    Npnts:=ResX*ResY;
    SetLength(Layer[LayerN].Mesh,Npnts);
    x:=-SizeX/2;
    y:=-SizeY/2;
    sx:=x;
    hx:=SizeX/(ResX);
    hy:=SizeY/(ResY);

    if R<>0 then begin //���� ���� �� �������, �� ��������������� ������� �������� ������ ��� Z
      mRotZ:=GetRotateMatrix('z',CylAxisAngle);
    end;

    for i:=0 to Npnts-1 do
    begin
      Layer[LayerN].Mesh[i][1]:=x;
      Layer[LayerN].Mesh[i][2]:=y;
      if R<>0 then begin
        vNode:=GetVector(x,y,0);
        vNode:=MatxVec(mRotZ,vNode);
        x1:=vNode[1];
        y1:=vNode[2];
        N:=exp(-(x*x+sqr(y-5))/(2*1))/(1*sqrt(2*Pi))/800;
        //if N>Nmax then Nmax:=N;
        //N:=0;
        Layer[LayerN].Mesh[i][3]:=sqrt(R*R-sqr(x1))-R+N+sqrt(R*R-sqr(y1));
      end
      else Layer[LayerN].Mesh[i][3]:=0;
      if ((i+1) mod ResX)<>0 then x:=x+hx
      else
      begin
        x:=sx;
        y:=y+hy;
      end;
      Layer[LayerN].Mesh[i][4]:=h;
    end;
    GetNormals(LayerN);
    //ShowMessage(FloatToStr(Nmax));
    FNumberOfNodes:=Npnts;
    result:=0;
  end;

  function TStructure.SetThicknessGradientY(LayerN: byte; HStart,HEnd: extended): integer;
  var
    i,Num: integer;
    y0,y1: extended;
  begin
    Num:=High(Layer[LayerN].Mesh);
    y0:=Layer[LayerN].Mesh[0][2];
    y1:=Layer[LayerN].Mesh[Num][2];
    for i:=0 to Num do
    begin
      Layer[LayerN].Mesh[i][4]:=(HEnd-HStart)*(Layer[LayerN].Mesh[i][2]-y0)/(y1-y0)+HStart;
    end;
    result:=0;
  end;

  function TStructure.SetThicknessGradientX(LayerN: byte; HStart,HEnd: extended): integer;
  var
    i,Num: integer;
    x0,x1: extended;
  begin
    Num:=High(Layer[LayerN].Mesh);
    x0:=Layer[LayerN].Mesh[0][1];
    x1:=Layer[LayerN].Mesh[Num][1];
    for i:=0 to Num do
    begin
      Layer[LayerN].Mesh[i][4]:=(HEnd-HStart)*(Layer[LayerN].Mesh[i][1]-x0)/(x1-x0)+HStart;
    end;
    result:=0;
  end;

  function TStructure.GetNormals(LayerN: byte): integer;
  var
    i,Num: integer;
    A,B: Vector;
    Dz: TDiff;
    //Fi: extended;
  begin
    Num:=High(Layer[LayerN].Mesh);
    SetLength(Layer[LayerN].vSurfaceNormals,Num+1);
     A[1]:=0.01; A[2]:=0;
     B[1]:=0; B[2]:=0.01;
    for i:=0 to Num do
    begin
      Dz:=Diff(LayerN,i);
      A[3]:=Dz.dZdX*0.01;
      B[3]:=Dz.dZdY*0.01;
      Layer[LayerN].vSurfaceNormals[i]:=VecxVec(A,B);
    end;
    result:=0;
  end;

  function TStructure.Diff(LayerN,pntN: integer): TDiff;
  var
    Ax,Ay: Matrix;
    Cx,Cy,Bx,By: Vector;
    j,n,Num: integer;
    h,x,y: extended;
  begin
      //h:=1e-12;
      x:=Layer[LayerN].Mesh[pntN][1];
      y:=Layer[LayerN].Mesh[pntN][2];
      n:=Round(Layer[LayerN].DPmmX*SizeX);
      Num:=High(Layer[LayerN].Mesh);
      //������ ����� �����
      if pntN=0 then
      begin
        for j:=1 to 3 do
        begin
          Ax[j][1]:=1;
          Ax[j][2]:=Layer[LayerN].Mesh[pntN+j-1][1];
          Ax[j][3]:=sqr(Layer[LayerN].Mesh[pntN+j-1][1]);
          Bx[j]:=Layer[LayerN].Mesh[pntN+j-1][3];

          Ay[j][1]:=1;
          Ay[j][2]:=Layer[LayerN].Mesh[pntN+(j-1)*n][2];
          Ay[j][3]:=sqr(Layer[LayerN].Mesh[pntN+(j-1)*n][2]);
          By[j]:=Layer[LayerN].Mesh[pntN+(j-1)*n][3];
        end;
      end else
      //������ ������ �����
      if pntN=n-1 then
      begin
        for j:=1 to 3 do
        begin
          Ax[j][1]:=1;
          Ax[j][2]:=Layer[LayerN].Mesh[pntN-j+1][1];
          Ax[j][3]:=sqr(Layer[LayerN].Mesh[pntN-j+1][1]);
          Bx[j]:=Layer[LayerN].Mesh[pntN-j+1][3];

          Ay[j][1]:=1;
          Ay[j][2]:=Layer[LayerN].Mesh[pntN+(j-1)*n][2];
          Ay[j][3]:=sqr(Layer[LayerN].Mesh[pntN+(j-1)*n][2]);
          By[j]:=Layer[LayerN].Mesh[pntN+(j-1)*n][3];
        end;
      end else
      //������� ����� �����
      if pntN=Num-n+1 then
      begin
        for j:=1 to 3 do
        begin
          Ax[j][1]:=1;
          Ax[j][2]:=Layer[LayerN].Mesh[pntN+j-1][1];
          Ax[j][3]:=sqr(Layer[LayerN].Mesh[pntN+j-1][1]);
          Bx[j]:=Layer[LayerN].Mesh[pntN+j-1][3];

          Ay[j][1]:=1;
          Ay[j][2]:=Layer[LayerN].Mesh[pntN-(j-1)*n][2];
          Ay[j][3]:=sqr(Layer[LayerN].Mesh[pntN-(j-1)*n][2]);
          By[j]:=Layer[LayerN].Mesh[pntN-(j-1)*n][3];
        end;
      end else
      //������� ������ �����
      if pntN=Num then
      begin
        for j:=1 to 3 do
        begin
          Ax[j][1]:=1;
          Ax[j][2]:=Layer[LayerN].Mesh[pntN-j+1][1];
          Ax[j][3]:=sqr(Layer[LayerN].Mesh[pntN-j+1][1]);
          Bx[j]:=Layer[LayerN].Mesh[pntN-j+1][3];

          Ay[j][1]:=1;
          Ay[j][2]:=Layer[LayerN].Mesh[pntN-(j-1)*n][2];
          Ay[j][3]:=sqr(Layer[LayerN].Mesh[pntN-(j-1)*n][2]);
          By[j]:=Layer[LayerN].Mesh[pntN-(j-1)*n][3];
        end;
      end else
      //������ � ��������� ������
      //���� ����� ����� �� ������ �����
      if (pntN>=1) and (pntN < n-1) then
      begin
        for j:=1 to 3 do
        begin
          Ax[j][1]:=1;
          Ax[j][2]:=Layer[LayerN].Mesh[pntN+j-2][1];
          Ax[j][3]:=sqr(Layer[LayerN].Mesh[pntN+j-2][1]);
          Bx[j]:=Layer[LayerN].Mesh[pntN+j-2][3];

          Ay[j][1]:=1;
          Ay[j][2]:=Layer[LayerN].Mesh[pntN+(j-1)*n][2];
          Ay[j][3]:=sqr(Layer[LayerN].Mesh[pntN+(j-1)*n][2]);
          By[j]:=Layer[LayerN].Mesh[pntN+(j-1)*n][3];
        end;
      end else
      //���� ����� ����� �� ����� �����
      if (((pntN+n) mod n)=0) and (pntN>0) and (pntN<Num+1-n)then
      begin
        for j:=1 to 3 do
        begin
          Ax[j][1]:=1;
          Ax[j][2]:=Layer[LayerN].Mesh[pntN+j-1][1];
          Ax[j][3]:=sqr(Layer[LayerN].Mesh[pntN+j-1][1]);
          Bx[j]:=Layer[LayerN].Mesh[pntN+j-1][3];

          Ay[j][1]:=1;
          Ay[j][2]:=Layer[LayerN].Mesh[pntN+(j-2)*n][2];
          Ay[j][3]:=sqr(Layer[LayerN].Mesh[pntN+(j-2)*n][2]);
          By[j]:=Layer[LayerN].Mesh[pntN+(j-2)*n][3];
        end;
      end else
      //���� ����� ����� �� ������ �����
      if (((pntN+n+1) mod n)=0) and (pntN>n-1) and (pntN<Num) then
      begin
        for j:=1 to 3 do
        begin
          Ax[j][1]:=1;
          Ax[j][2]:=Layer[LayerN].Mesh[pntN-j+1][1];
          Ax[j][3]:=sqr(Layer[LayerN].Mesh[pntN-j+1][1]);
          Bx[j]:=Layer[LayerN].Mesh[pntN-j+1][3];

          Ay[j][1]:=1;
          Ay[j][2]:=Layer[LayerN].Mesh[pntN+(j-2)*n][2];
          Ay[j][3]:=sqr(Layer[LayerN].Mesh[pntN+(j-2)*n][2]);
          By[j]:=Layer[LayerN].Mesh[pntN+(j-2)*n][3];
        end;
      end else
      //���� ����� ����� �� ������� �����
      if (pntN>Num+1-n) and (pntN<Num) then
      begin
        for j:=1 to 3 do
        begin
          Ax[j][1]:=1;
          Ax[j][2]:=Layer[LayerN].Mesh[pntN+j-2][1];
          Ax[j][3]:=sqr(Layer[LayerN].Mesh[pntN+j-2][1]);
          Bx[j]:=Layer[LayerN].Mesh[pntN+j-2][3];

          Ay[j][1]:=1;
          Ay[j][2]:=Layer[LayerN].Mesh[pntN-(j-1)*n][2];
          Ay[j][3]:=sqr(Layer[LayerN].Mesh[pntN-(j-1)*n][2]);
          By[j]:=Layer[LayerN].Mesh[pntN-(j-1)*n][3];
        end;
      end
      else begin
        for j:=1 to 3 do
        begin
          Ax[j][1]:=1;
          Ax[j][2]:=Layer[LayerN].Mesh[pntN+j-2][1];
          Ax[j][3]:=sqr(Layer[LayerN].Mesh[pntN+j-2][1]);
          Bx[j]:=Layer[LayerN].Mesh[pntN+j-2][3];

          Ay[j][1]:=1;
          Ay[j][2]:=Layer[LayerN].Mesh[pntN+(j-2)*n][2];
          Ay[j][3]:=sqr(Layer[LayerN].Mesh[pntN+(j-2)*n][2]);
          By[j]:=Layer[LayerN].Mesh[pntN+(j-2)*n][3];
        end;
      end;
      Cx:=SLAU(Ax,Bx);
      //result.dZdX:=((Cx[1]+Cx[2]*(x+h/2)+Cx[3]*sqr(x+h/2))-(Cx[1]+Cx[2]*(x-h/2)+Cx[3]*sqr(x-h/2)))/h;
      result.dZdX:=Cx[2]+2*Cx[3]*x;
      Cy:=SLAU(Ay,By);
      //result.dZdY:=((Cy[1]+Cy[2]*(y+h/2)+Cy[3]*sqr(y+h/2))-(Cy[1]+Cy[2]*(y-h/2)+Cy[3]*sqr(y-h/2)))/h;
      result.dZdY:=Cy[2]+2*Cy[3]*y;
  end;

  function TStructure.SetLayerMisorientation(LayerN: byte; Value,From,Upto: extended; Dir: Char{x or y}): integer;
  var
    mRotY: Matrix;
    pntN: integer;
  begin
    if Dir='y' then begin
      From:=From+10;
      Upto:=Upto+10;
      mRotY:=GetRotateMatrix('y',Value);
      for pntN:=abs(Round(From*Layer[LayerN].DpmmY*SizeX*Layer[LayerN].DPmmX)) to abs(Round(Upto*Layer[LayerN].DpmmY*SizeX*Layer[LayerN].DPmmX)) do begin
        Layer[LayerN].vSurfaceNormals[pntN]:=MatxVec(mRotY,Layer[LayerN].vSurfaceNormals[pntN]);
      end;
    end;
    Result:=0;
  end;

//TXRay
  constructor TXRay.Create();
  begin
    inherited Create;
    SetLambda(taCuKalfa1);
    SetSourceParams(100000,0,48);
  end;

  function TXRay.LabmdaOf(Anod: TXRayTubeAnod): extended;
  begin
    case Anod of
      taAgKalfa1: Result:=mx_StrToExt('0'+DecimalSeparator+'5594075');
      taAgKalfa2: Result:=mx_StrToExt('0'+DecimalSeparator+'563798');
      taAgKbeta1: Result:=mx_StrToExt('0'+DecimalSeparator+'497069');
      taAgKbeta2: Result:=mx_StrToExt('0'+DecimalSeparator+'497685');
      taCoKalfa1: Result:=mx_StrToExt('1'+DecimalSeparator+'788965');
      taCoKalfa2: Result:=mx_StrToExt('1'+DecimalSeparator+'792850');
      taCoKbeta1: Result:=mx_StrToExt('1'+DecimalSeparator+'620790');
      taCrKalfa1: Result:=mx_StrToExt('2'+DecimalSeparator+'289700');
      taCrKalfa2: Result:=mx_StrToExt('2'+DecimalSeparator+'293606');
      taCrKbeta1: Result:=mx_StrToExt('2'+DecimalSeparator+'084870');
      taCuKalfa1: Result:=mx_StrToExt('1'+DecimalSeparator+'540562');
      taCuKalfa2: Result:=mx_StrToExt('1'+DecimalSeparator+'544398');
      taCuKbeta1: Result:=mx_StrToExt('1'+DecimalSeparator+'392218');
    else
      result:=0;
    end;
  end;

  procedure TXRay.SetLambda(Anod: TXRayTubeAnod);
  begin
    Lambda:=LabmdaOf(Anod);
  end;

  procedure TXRay.SetSourceParams(HSize,VSize,DistanceToWin: extended);
  begin
    VSourseSize:=VSize;
    HSourceSize:=HSize;
    LWinToSource:=DistanceToWin;
  end;

  procedure TXRay.SetWinParams(HSize,VSize: extended; Position: TWinPosition = wpMiddle);
  begin
    HWinSize:=HSize;
    VWinSize:=VSize;
    case Position of
      wpMiddle: begin
        HSpread:=0;
        VSpread:=3.5;
      end;
    else
      HSpread:=0;
      VSpread:=3.5;
    end;
  end;

//TXRayTopo
  constructor TXRayTopo.Create;
  begin
    inherited Create;
    SetLWinToStruct(7);
  end;
  destructor TXRayTopo.Destroy;
  begin
    if Assigned(XRay) then XRay.Free;
    if Assigned(Structure) then Structure.Free;
    inherited Destroy;
  end;

  function TXRayTopo.GetTeta0(LayerN: byte): extended;
  begin
    Result:=Structure.Layer[LayerN].BraggAngle(Reflex[1],Reflex[2],Reflex[3],XRay.Lambda);
  end;

  function TXRayTopo.GetFi0(LayerN: byte): extended;
  begin
    Result:=Structure.Layer[LayerN].GetFi0(Reflex[1],Reflex[2],Reflex[3]);
  end;

  procedure TXRayTopo.SetLWinToStruct(L: extended);
  begin
    LWinToStruct:=L;
  end;

  function TXRayTopo.AddStructure(SizeX,SizeY: extended; NLayers: byte = 3): integer;
  begin
    if not Assigned(Structure) then
      Structure:=TStructure.Create(SizeX,SizeY,NLayers)
    else
    begin
      MessageBox(0,'Structure already exists','Error',MB_ICONERROR);
      Application.Terminate;
    end;
    result:=0;
  end;

  function TXRayTopo.DeleteStructure(): integer;
  begin
    if Assigned(Structure) then
      Structure.Free
    else
    begin
      MessageBox(0,'Structure is not exists','Error',MB_ICONERROR);
      Application.Terminate;
    end;
    result:=0;
  end;

  function TXRayTopo.AddXRay(): integer;
  begin
    if not Assigned(XRay) then
      XRay:=TXRay.Create
    else
    begin
      MessageBox(0,'XRay already exists','Error',MB_ICONERROR);
      Application.Terminate;
    end;
    result:=0;
  end;

  function TXRayTopo.DeleteXRay(): integer;
  begin
    if Assigned(XRay) then
      XRay.Free
    else
    begin
      MessageBox(0,'DeleteXRay: XRay is not exists','Error',MB_ICONERROR);
      Application.Terminate;
    end;
    result:=0;
  end;

  function TXRayTopo.SetCenterPoint(x,y: extended): integer;
  //ToDo -cSetCenterPoint: ���������� � ������ �������
  var
    i,n: integer;
  begin
    if (x=0) and (y=0) then begin result:=0; exit; end;
    if not (Assigned(XRay) and Assigned(Structure)) then
    begin
      MessageBox(0,'XRay or Structure is not exists','Error',MB_ICONERROR);
      Application.Terminate;
    end;
    if (2*Abs(x)>Structure.SizeX) or (2*Abs(y)>Structure.SizeY) then
    begin
      MessageBox(0,'Point is out of Structure','Error',MB_ICONERROR);
      Application.Terminate;
    end;

    for n:=1 to Structure.NLayers do
    begin
      for i:=0 to High(Structure.Layer[n].Mesh) do
      begin
        Structure.Layer[n].Mesh[i][1]:=Structure.Layer[n].Mesh[i][1]-x;
        Structure.Layer[n].Mesh[i][2]:=Structure.Layer[n].Mesh[i][2]-y;
      end;
    end;
    result:=0;
  end;

  procedure TXRayTopo.SetLayerParams(LayerN: Byte; LatticeParametr,EpsilonZ: extended; vDirY: Vector; Relaxation: extended = 0);
  begin
    Structure.SetLayerParams(LayerN, LatticeParametr, EpsilonZ, vDirY, Relaxation);
  end;

  function TXRayTopo.SetCylLayerProfil(LayerN: byte; R,h: extended; DotsPerMMX: integer = 10; DotsPerMMY: integer = 10; CylAxisAngle: extended = 0): integer;
  begin
    Structure.SetCylLayerProfil(LayerN,R,h,DotsPerMMX,DotsPerMMY,CylAxisAngle);
  end;

  function TXRayTopo.SetThicknessGradientY(LayerN: byte; HStart,HEnd: extended): integer;
  begin
    Structure.SetThicknessGradientY(LayerN,HStart,HEnd);
  end;

  function TXRayTopo.SetThicknessGradientX(LayerN: byte; HStart,HEnd: extended): integer;
  begin
    Structure.SetThicknessGradientX(LayerN,HStart,HEnd);
  end;

  function TXRayTopo.SetLayerMisorientation(LayerN: byte; Value,From,Upto: extended; Dir: Char{x or y}): integer;
  begin
    Structure.SetLayerMisorientation(LayerN,Value,From,Upto,Dir);
  end;


  function TXRayTopo.Diff(LayerN,pntN: integer): TDiff;
  var
    Ax,Ay: Matrix;
    Cx,Cy,Bx,By: Vector;
    j,n,Num: integer;
    h,x,y: extended;
  begin
      //h:=1e-12;
      x:=Structure.Layer[LayerN].Mesh[pntN][1];
      y:=Structure.Layer[LayerN].Mesh[pntN][2];
      n:=Round(Structure.Layer[LayerN].DPmmX*Structure.SizeX);
      Num:=High(Structure.Layer[LayerN].Mesh);
      //������ ����� �����
      if pntN=0 then
      begin
        for j:=1 to 3 do
        begin
          Ax[j][1]:=1;
          Ax[j][2]:=Structure.Layer[LayerN].Mesh[pntN+j-1][1];
          Ax[j][3]:=sqr(Structure.Layer[LayerN].Mesh[pntN+j-1][1]);
          Bx[j]:=Structure.Layer[LayerN].Mesh[pntN+j-1][3];

          Ay[j][1]:=1;
          Ay[j][2]:=Structure.Layer[LayerN].Mesh[pntN+(j-1)*n][2];
          Ay[j][3]:=sqr(Structure.Layer[LayerN].Mesh[pntN+(j-1)*n][2]);
          By[j]:=Structure.Layer[LayerN].Mesh[pntN+(j-1)*n][3];
        end;
      end else
      //������ ������ �����
      if pntN=n-1 then
      begin
        for j:=1 to 3 do
        begin
          Ax[j][1]:=1;
          Ax[j][2]:=Structure.Layer[LayerN].Mesh[pntN-j+1][1];
          Ax[j][3]:=sqr(Structure.Layer[LayerN].Mesh[pntN-j+1][1]);
          Bx[j]:=Structure.Layer[LayerN].Mesh[pntN-j+1][3];

          Ay[j][1]:=1;
          Ay[j][2]:=Structure.Layer[LayerN].Mesh[pntN+(j-1)*n][2];
          Ay[j][3]:=sqr(Structure.Layer[LayerN].Mesh[pntN+(j-1)*n][2]);
          By[j]:=Structure.Layer[LayerN].Mesh[pntN+(j-1)*n][3];
        end;
      end else
      //������� ����� �����
      if pntN=Num-n+1 then
      begin
        for j:=1 to 3 do
        begin
          Ax[j][1]:=1;
          Ax[j][2]:=Structure.Layer[LayerN].Mesh[pntN+j-1][1];
          Ax[j][3]:=sqr(Structure.Layer[LayerN].Mesh[pntN+j-1][1]);
          Bx[j]:=Structure.Layer[LayerN].Mesh[pntN+j-1][3];

          Ay[j][1]:=1;
          Ay[j][2]:=Structure.Layer[LayerN].Mesh[pntN-(j-1)*n][2];
          Ay[j][3]:=sqr(Structure.Layer[LayerN].Mesh[pntN-(j-1)*n][2]);
          By[j]:=Structure.Layer[LayerN].Mesh[pntN-(j-1)*n][3];
        end;
      end else
      //������� ������ �����
      if pntN=Num then
      begin
        for j:=1 to 3 do
        begin
          Ax[j][1]:=1;
          Ax[j][2]:=Structure.Layer[LayerN].Mesh[pntN-j+1][1];
          Ax[j][3]:=sqr(Structure.Layer[LayerN].Mesh[pntN-j+1][1]);
          Bx[j]:=Structure.Layer[LayerN].Mesh[pntN-j+1][3];

          Ay[j][1]:=1;
          Ay[j][2]:=Structure.Layer[LayerN].Mesh[pntN-(j-1)*n][2];
          Ay[j][3]:=sqr(Structure.Layer[LayerN].Mesh[pntN-(j-1)*n][2]);
          By[j]:=Structure.Layer[LayerN].Mesh[pntN-(j-1)*n][3];
        end;
      end else
      //������ � ��������� ������
      //���� ����� ����� �� ������ �����
      if (pntN>=1) and (pntN < n-1) then
      begin
        for j:=1 to 3 do
        begin
          Ax[j][1]:=1;
          Ax[j][2]:=Structure.Layer[LayerN].Mesh[pntN+j-2][1];
          Ax[j][3]:=sqr(Structure.Layer[LayerN].Mesh[pntN+j-2][1]);
          Bx[j]:=Structure.Layer[LayerN].Mesh[pntN+j-2][3];

          Ay[j][1]:=1;
          Ay[j][2]:=Structure.Layer[LayerN].Mesh[pntN+(j-1)*n][2];
          Ay[j][3]:=sqr(Structure.Layer[LayerN].Mesh[pntN+(j-1)*n][2]);
          By[j]:=Structure.Layer[LayerN].Mesh[pntN+(j-1)*n][3];
        end;
      end else
      //���� ����� ����� �� ����� �����
      if (((pntN+n) mod n)=0) and (pntN>0) and (pntN<Num+1-n)then
      begin
        for j:=1 to 3 do
        begin
          Ax[j][1]:=1;
          Ax[j][2]:=Structure.Layer[LayerN].Mesh[pntN+j-1][1];
          Ax[j][3]:=sqr(Structure.Layer[LayerN].Mesh[pntN+j-1][1]);
          Bx[j]:=Structure.Layer[LayerN].Mesh[pntN+j-1][3];

          Ay[j][1]:=1;
          Ay[j][2]:=Structure.Layer[LayerN].Mesh[pntN+(j-2)*n][2];
          Ay[j][3]:=sqr(Structure.Layer[LayerN].Mesh[pntN+(j-2)*n][2]);
          By[j]:=Structure.Layer[LayerN].Mesh[pntN+(j-2)*n][3];
        end;
      end else
      //���� ����� ����� �� ������ �����
      if (((pntN+n+1) mod n)=0) and (pntN>n-1) and (pntN<Num) then
      begin
        for j:=1 to 3 do
        begin
          Ax[j][1]:=1;
          Ax[j][2]:=Structure.Layer[LayerN].Mesh[pntN-j+1][1];
          Ax[j][3]:=sqr(Structure.Layer[LayerN].Mesh[pntN-j+1][1]);
          Bx[j]:=Structure.Layer[LayerN].Mesh[pntN-j+1][3];

          Ay[j][1]:=1;
          Ay[j][2]:=Structure.Layer[LayerN].Mesh[pntN+(j-2)*n][2];
          Ay[j][3]:=sqr(Structure.Layer[LayerN].Mesh[pntN+(j-2)*n][2]);
          By[j]:=Structure.Layer[LayerN].Mesh[pntN+(j-2)*n][3];
        end;
      end else
      //���� ����� ����� �� ������� �����
      if (pntN>Num+1-n) and (pntN<Num) then
      begin
        for j:=1 to 3 do
        begin
          Ax[j][1]:=1;
          Ax[j][2]:=Structure.Layer[LayerN].Mesh[pntN+j-2][1];
          Ax[j][3]:=sqr(Structure.Layer[LayerN].Mesh[pntN+j-2][1]);
          Bx[j]:=Structure.Layer[LayerN].Mesh[pntN+j-2][3];

          Ay[j][1]:=1;
          Ay[j][2]:=Structure.Layer[LayerN].Mesh[pntN-(j-1)*n][2];
          Ay[j][3]:=sqr(Structure.Layer[LayerN].Mesh[pntN-(j-1)*n][2]);
          By[j]:=Structure.Layer[LayerN].Mesh[pntN-(j-1)*n][3];
        end;
      end
      else begin
        for j:=1 to 3 do
        begin
          Ax[j][1]:=1;
          Ax[j][2]:=Structure.Layer[LayerN].Mesh[pntN+j-2][1];
          Ax[j][3]:=sqr(Structure.Layer[LayerN].Mesh[pntN+j-2][1]);
          Bx[j]:=Structure.Layer[LayerN].Mesh[pntN+j-2][3];

          Ay[j][1]:=1;
          Ay[j][2]:=Structure.Layer[LayerN].Mesh[pntN+(j-2)*n][2];
          Ay[j][3]:=sqr(Structure.Layer[LayerN].Mesh[pntN+(j-2)*n][2]);
          By[j]:=Structure.Layer[LayerN].Mesh[pntN+(j-2)*n][3];
        end;
      end;
      Cx:=SLAU(Ax,Bx);
      //result.dZdX:=((Cx[1]+Cx[2]*(x+h/2)+Cx[3]*sqr(x+h/2))-(Cx[1]+Cx[2]*(x-h/2)+Cx[3]*sqr(x-h/2)))/h;
      result.dZdX:=Cx[2]+2*Cx[3]*x;
      Cy:=SLAU(Ay,By);
      //result.dZdY:=((Cy[1]+Cy[2]*(y+h/2)+Cy[3]*sqr(y+h/2))-(Cy[1]+Cy[2]*(y-h/2)+Cy[3]*sqr(y-h/2)))/h;
      result.dZdY:=Cy[2]+2*Cy[3]*y;
  end;

  function TXRayTopo.GetRayDir(LayerN,pntN: integer): Vector;
  var
    D: Vector;
    mRotY,mRotZ: Matrix;
    //x,y,
    z,Teta: extended;
  begin
    //x:=Structure.Layer[LayerN].Mesh[pntN][1]; //������������ ����� ���������� ������������ �� x
    //y:=Structure.Layer[LayerN].Mesh[pntN][2]; //������������ ����� ���������� ������������ �� y
    z:=Structure.Layer[LayerN].Mesh[pntN][3];
    D[1]:=0; //� ������ �� ������������� �� ����������� �����
    D[2]:=0;//-y;
    D[3]:=XRay.LWinToSource+LWinToStruct{-z};

    //������� �������� ������ ��� y, ��� ������� ���� ������� �������� ����
    Teta:=Pi/2-(Teta0+dTeta0-GetFi0(1));
    mRotY:=GetRotateMatrix('Y',Teta{*180/Pi:Todo1});
    //
    result:=MatxVec(mRotY,D);
  end;

  function TXRayTopo.GetFallAngle(LayerN,pntN: integer): extended;
  var
    Num,i: integer;
    vBeam,Axis,Normal: Vector;
    F,Fi0: extended;
  begin
    //Num:=High(Normals);
    //SetLength(FallAngles,Num+1);
    Fi0:=GetFi0(LayerN);
    vBeam:=GetRayDir(LayerN,0);
    Normal:=Structure.Layer[LayerN].vSurfaceNormals[pntN];

    //���� ��� ���������� ������� � ��������� ��������� � ������ �� ���� ��������� Fi0
    if Fi0<>0 then begin
      //for i:=0 to Num do begin
        //Axis:=VecxVec(vOsZ,Normal);
        //Normal:=RotateVector(Axis,vReflexDir,AngleBtwnVectors(Normal,vOsZ));
        Axis:=VecxVec(Normal,vReflexDir);
        Normal:=RotateVector(Axis,Normal,Fi0);
      //end;
    end;
    Result:=Pi/2-AngleBtwnVectors(vBeam,Normal);
    
  end;

  function TXRayTopo.SetStructureOrientation(h,k,l, Azimut, Gamma: extended; dTeta: extended = 0): integer;
  var
    mRotZ,mRotX: Matrix;
    Num,i,LN: integer;
    Alpha: extended;
    Lam: extended;
    fStructFacts: TextFile;
    StructFacts: DynArray;
    strFacts: string;
    vOsX: Vector;
  begin
    vOsX:=GetVector(1,0,0);
    for LN:=1 to Structure.NLayers do begin
      //Structure.GetNormals(LN);
      if not IsNullVector(Structure.Layer[LN].vDirAxisY) then begin
        Alpha:=AngleBtwnVectors(Structure.Layer[LN].vDirAxisY,GetVector(0,1,0));
        if Alpha<>0 then
        begin
          Num:=High(Structure.Layer[LN].Mesh);
          if Structure.Layer[LN].vDirAxisY[1]>=0 then mRotZ:=GetRotateMatrix('z',-Alpha)
          else mRotZ:=GetRotateMatrix('z',Alpha);
          for i:=0 to Num do begin
            Structure.Layer[LN].vSurfaceNormals[i]:=MatxVec(mRotZ,Structure.Layer[LN].vSurfaceNormals[i]);
          end;
          Azimut0:=Alpha;
        end;
      end;
    end;
    vOsX:=MatxVec(mRotZ,vOsX);

    Reflex:=GetVector(h,k,l);
    Num:=High(Structure.Layer[1].vSurfaceNormals);
    dTeta0:=dTeta;
    Teta0:=GetTeta0(1);
    //Fi0:=GetFi0(1);
    //����� ��������������� �������� � ����������� �� �������
    if cos(Azimut0-Azimut)>=0 then Reflex[1]:=abs(Reflex[1])
    else Reflex[1]:=-abs(Reflex[1]);
    if sin(Azimut0-Azimut)>=0 then Reflex[2]:=abs(Reflex[1])
    else Reflex[2]:=-abs(Reflex[1]);
    vReflexDir:=Reflex;
    vOsZ:=GetVector(0,0,1);
    //������� �������� � ������ �������
    if Azimut<>Azimut0 then begin
      mRotZ:=GetRotateMatrix('z',Azimut-Azimut0);
      for LN:=1 to Structure.NLayers do begin
            for i:=0 to Num do begin
                Structure.Layer[LN].vSurfaceNormals[i]:=MatxVec(mRotZ,Structure.Layer[LN].vSurfaceNormals[i]);
            end;
      end;
      vReflexDir:=MatxVec(mRotZ,vReflexDir);
      vOsZ:=MatxVec(mRotZ,vOsZ);
      vOsX:=MatxVec(mRotZ,vOsX);
      Azimut0:=Azimut;
    end;
    //������� �������� � ������ ���� �����
    if Gamma<>0 then begin
      mRotX:=GetRotateMatrix('x',Gamma);
      mRotX:=GetRotateMatrix(vOsX,Gamma);
      for LN:=1 to Structure.NLayers do begin
              for i:=0 to Num do begin
                Structure.Layer[LN].vSurfaceNormals[i]:=MatxVec(mRotX,Structure.Layer[LN].vSurfaceNormals[i]);
              end;
      end;
      vReflexDir:=MatxVec(mRotX,vReflexDir);
      vOsZ:=MatxVec(mRotX,vOsZ);
    end;
     { TODO : ���������� ��������� ��������� ������� ����� }
    //
    //GetFallAngles(1);
    //����������� ����� ���� � ������������� ���������� ��� �����
    SetLength(Q,Structure.NLayers+1);
    SetLength(G0,Structure.NLayers+1);
    SetLength(Gh,Structure.NLayers+1);
    SetLength(b_,Structure.NLayers+1);
    //Lam:=XRay.Lambda;
    for i:=1 to Structure.NLayers do
    begin
      Q[i]:=GetTeta0(i);
      G0[i]:=sin(Q[i]-GetFi0(i));
      Gh[i]:=-sin(Q[i]+GetFi0(i));
      b_[i]:=G0[i]/Gh[i];
    end;
    //
    //����� ����������� ��������, � ����������� �� �������������� ��������
    Assign(fStructFacts,'structfact.txt');
    Reset(fStructFacts);
    While not Eof(fStructFacts) do begin
        Readln(fStructFacts,strFacts);
        StructFacts:=mxExplode(';',strFacts);
        if (mx_StrToExt(StructFacts[1])=abs(Reflex[1])) and (mx_StrToExt(StructFacts[2])=abs(Reflex[2])) and (mx_StrToExt(StructFacts[3])=abs(Reflex[3])) then begin
                Psi01:=mx_StrToExt(StructFacts[4]);
                Psi011:=mx_StrToExt(StructFacts[5]);
                Psih1:=mx_StrToExt(StructFacts[6]);
                Psih11:=mx_StrToExt(StructFacts[7]);
                Break;
        end;
        if Eof(fStructFacts) then begin
                MessageBox(0,'��� ��������� �������� ����������� ����������� �������. ������ ����������','Error',MB_ICONERROR);
                Application.Terminate;
        end;
    end;
    CloseFile(fStructFacts);
    //
    result:=0;
  end;

  function TXRayTopo.GetIntensity(): integer;
  var
    Rp,D,E,F,eps,ta,t,dhkl,yy,psi0,psi4,teta,tetaB,lambda,y: extended;
    Num,i: integer;
  begin
    Num:=High(Structure.Layer[1].vSurfaceNormals);
    SetLength(Intensity,Num+1);
    eps:=Structure.Layer[2].EpsZ;
    dhkl:=Structure.Layer[1].Dhkl(Reflex[1],Reflex[2],Reflex[3]);
    tetaB:=Teta0;
    lambda:=XRay.Lambda;
    psi4:=7.63E-06;
    psi0:= 1.51E-05;

    for i:=0 to Num do
    begin
      ta:=Structure.Layer[2].Mesh[i][4];
      t:=Structure.Layer[3].Mesh[i][4];
      F:=2*PI*eps*ta/dhkl;
      teta:=FallAngles[i]-Teta0;
      yy:=2*PI*(psi0+teta*sin(2*tetaB))/(lambda*sin(tetaB));
      D:= yy*(t+ta);
      E:=yy*ta;
      y:=(psi0+(teta)*sin(2*tetaB))/psi4;
       if (y<-1) then
         Rp:=sqr(y+sqrt(y*y-1))
       else if (y>1) then Rp:=sqr(y-sqrt(y*y-1))
       else Rp:=1;
      Intensity[i]:=Rp*(3+2*cos(F)*(cos(D)-Cos(E))+2*sin(F)*(Sin(D)-Sin(E))-2*cos(D-E));
    end;
    Result:=0;
  end;

  function TXRayTopo.GetIntensityTT(): integer;
  var
    Num,i,pnt: integer;
    Lam, AlphaH, g, y, C, P, K, Psih1,Psih11,Psi01,Psi011: extended;
    A,B, X0,
    T1{T1 - ����������� ��������},T3{T1*C*(z-z0)}:complex;
    z1{������ T1},z11{������ ������},
    fi1{��������� T1},fi11{��������� ������}: extended;
    Q,G0,Gh, b_: array of extended;
    //
    //LogFile: TextFile;
    //logstr: string;

    function GetNewX(Teta: extended): extended;
    begin
      K:=Psih11/Psih1;
      A.r:=1; A.i:=K;
      C:=Pi*Psih1/sqrt(abs(G0[i]*Gh[i]))/XRay.Lambda;
      AlphaH:=2*(Q[i]-Teta)*sin(2*Q[i]); //�������� ����������
      //AlphaH:=(G0[i]*(G0[i]-Gh[i]-2*sin(Q[i]*cos(Fi0)))+Psi01*(1-b_[i])/2) / (sqrt(abs(b_[i]))*1*sqrt(sqr(Psih1)+sqr(Psih11)));
      g:=(1-b_[i])*Psi011/(2*sqrt(abs(b_[i]))*P*abs(Psih1));
      y:=((1-b_[i])*Psi01+b_[i]*AlphaH)/(2*sqrt(abs(b_[i]))*P*abs(Psih1));
      B.r:=y; B.i:=g;

      T1:=cdif(cmul(B,B),cmul(A,A));
      z1:=sqrt(sqr(T1.r)+sqr(T1.i));
      fi1:=ArcTan(T1.i/T1.r);
      z11:=sqrt(z1);
      fi11:=fi1/2;
      T1.r:=z11*cos(fi11); T1.i:=z11*sin(fi11);

      if (X0.r=CNul.r)and(X0.i=Cnul.i) then
      begin
        if y>1-abs(g) then begin
          X0:=cdiv(cdif(B,T1),A);
        end
        else if y<-1+abs(g) then begin
          X0:=cdiv(csum(B,T1),A);
        end
        else begin
          X0.r:=1-abs(g);
        end;
        {if T1.i>0 then X0:=cdiv(cdif(B,T1),A)
        else X0:=cdiv(csum(B,T1),A);}
      end else
      begin
        T3.r:=C*T1.r*Structure.Layer[i].Mesh[pnt][4];
        T3.i:=C*T1.i*Structure.Layer[i].Mesh[pnt][4];
        X0:=cdiv(csum(  cmul(X0,T1),   cmul(cmul(ctan(T3),cdif(A,cmul(X0,B))), CIm1)),    cdif(T1,cmul(cmul(ctan(T3),cdif(cmul(A,X0),B)),CIm1)));
      end;

      Result:=sqr(X0.r)+sqr(X0.i);
    end;

  begin
    begin
    end;
    Num:=High(Structure.Layer[1].vSurfaceNormals);
    SetLength(Intensity, Num+1);
//������ �������
    SetLength(Q,Structure.NLayers+1);
    SetLength(G0,Structure.NLayers+1);
    SetLength(Gh,Structure.NLayers+1);
    SetLength(b_,Structure.NLayers+1);
    //Lam:=XRay.Lambda;
    for i:=1 to Structure.NLayers do
    begin
      Q[i]:=GetTeta0(i);
      G0[i]:=sin(Q[i]-GetFi0(i));
      Gh[i]:=-sin(Q[i]+GetFi0(i));
      b_[i]:=G0[i]/Gh[i];
    end;
    P:=1; //������ �����������
    X0:=CNul;
    for pnt:=0 to Num do
    begin
      X0:=CNul;
      for i:=1 to Structure.NLayers do
      begin
  //      Fi0:=GetFi0(i);
        if Reflex[1]=0 then begin
          Psi01:=-0.15127E-04; Psi011:=0.34955E-06; Psih1:=-0.76339E-05; Psih11:=-0.32018E-06; //��� 004 ��� Si
        end
        else if abs(Reflex[1])=2 then begin
          Psi01:=-0.15127E-04; Psi011:=0.34955E-06; Psih1:=-0.65879E-05; Psih11:=-0.30642E-06; //��� 224 ��� Si
        end
        else if abs(Reflex[1])=1 then begin
          Psi01:=-0.15127E-04; Psi011:=0.34955E-06; Psih1:=-0.59956E-05; Psih11:=-0.23270E-06; //��� 113 ��� Si
        end;
        //Psi01:=-0.28830E-04; Psi011:=0.81730E-06; Psih1:=0.14449E-04; Psih11:=0.73531E-06; //��� 224 ��� Ge
        {if i=2 then begin
          Psi01:=Psi01*0.01;
          Psi011:=Psi011;
          Psih1:=Psih1*0.01;
          Psih11:=Psih11;
        end;}
        
        GetNewX(GetFallAngle(i,pnt)); //����������� ��������� �������� ���������� ����� � ��������
        if i=Structure.NLayers then Intensity[pnt]:=sqr(X0.r)+sqr(X0.i)
      end;
      //����� ��� ��� ������������ �������
    end;
    Result:=0;
  end;

  function TXRayTopo.GetX_Takagi(LayerN: byte; pnt: integer; X0: complex; Teta: extended): complex;
  var
    AlphaH, g, y, C, P, K: extended;
    A,B,
    T1{T1 - ����������� ��������},T3{T1*C*(z-z0)}:complex;
    z1{������ T1},z11{������ ������},
    fi1{��������� T1},fi11{��������� ������}: extended;
  begin
      P:=1;  
      K:=Psih11/Psih1;
      A.r:=1; A.i:=K;
      C:=Pi*Psih1/sqrt(abs(G0[LayerN]*Gh[LayerN]))/XRay.Lambda;
      AlphaH:=2*(Q[LayerN]-Teta)*sin(2*Q[LayerN]); //�������� ����������
      //AlphaH:=(G0[i]*(G0[i]-Gh[i]-2*sin(Q[i]*cos(Fi0)))+Psi01*(1-b_[i])/2) / (sqrt(abs(b_[i]))*1*sqrt(sqr(Psih1)+sqr(Psih11)));
      g:=(1-b_[LayerN])*Psi011/(2*sqrt(abs(b_[LayerN]))*P*abs(Psih1));
      y:=((1-b_[LayerN])*Psi01+b_[LayerN]*AlphaH)/(2*sqrt(abs(b_[LayerN]))*P*abs(Psih1));
      B.r:=y; B.i:=g;

      T1:=cdif(cmul(B,B),cmul(A,A));
      z1:=sqrt(sqr(T1.r)+sqr(T1.i));
      fi1:=ArcTan(T1.i/T1.r);
      z11:=sqrt(z1);
      fi11:=fi1/2;
      T1.r:=z11*cos(fi11); T1.i:=z11*sin(fi11);

      if (X0.r=CNul.r)and(X0.i=Cnul.i) then
      begin
        if y>1-abs(g) then begin
          Result:=cdiv(cdif(B,T1),A);
        end
        else if y<-1+abs(g) then begin
          Result:=cdiv(csum(B,T1),A);
        end
        else begin
          Result.r:=1-abs(g);
        end;
        {if T1.i>0 then X0:=cdiv(cdif(B,T1),A)
        else X0:=cdiv(csum(B,T1),A);}
      end else
      begin
        T3.r:=C*T1.r*Structure.Layer[LayerN].Mesh[pnt][4];
        T3.i:=C*T1.i*Structure.Layer[LayerN].Mesh[pnt][4];
        Result:=cdiv(csum(  cmul(X0,T1),   cmul(cmul(ctan(T3),cdif(A,cmul(X0,B))), CIm1)),    cdif(T1,cmul(cmul(ctan(T3),cdif(cmul(A,X0),B)),CIm1)));
      end;
  end;

  function TXRayTopo.GetIinNode(Node: integer; K0: Vector): extended;
  var
    i: byte; //��� ������� �� �����
    X0: complex;
  begin
    X0:=CNul;
    for i:=1 to Structure.NLayers do begin
        X0:=GetX_Takagi(i,Node,X0,GetFallAngle(i,Node)); //����������� ��������� �������� ���������� ����� � ��������
    end;
    Result:=sqr(X0.r)+sqr(X0.i);
  end;
  
  function TXRayTopo.GetIntensityTT2: integer;
  var
    pnt: integer;
    K0: Vector;
  begin
    SetLength(Intensity, Structure.FNumberOfNodes);
    for pnt:=0 to Structure.FNumberOfNodes-1 do begin
      Intensity[pnt]:=GetIinNode(pnt,K0);
    end; 
  end;

  function TXRayTopo.GetKDO(t: extended): integer;
  var
    KDOFile: TextFile;
    str: string;
    stop, Num,pnt: integer;
    MinDt: extended;
  begin
    AssignFile(KDOFile,'kdo.dat');
    ReWrite(KDOFile);
    Num:=High(Structure.Layer[1].vSurfaceNormals);
    MinDt:=MaxExtended;
    for pnt:=0 to Num do begin
        if (t-Structure.Layer[2].Mesh[pnt][4]<MinDt) then MinDt:=Structure.Layer[2].Mesh[pnt][4];
    end;
    stop:=0;
    for pnt:=0 to Num do begin
        if (Structure.Layer[2].Mesh[pnt][4]=MinDt) and (stop<Structure.SizeX*Structure.Layer[2].DPmmX) then begin
            str:=FloatToStr((GetFallAngle(1,pnt))*180/Pi*3600-GetTeta0(1)*180/Pi*3600)+';'+FloatToStr(Intensity[pnt]);
            Writeln(KDOFile,str);
            Inc(stop);
        end;
    end;
    CloseFile(KDOFile);
  end;

  function TXRayTopo.GetReflectedVectors(): integer;
  var
    Num,i: integer;
    Kh: Vector; //������ ���������
  begin
    Num:=High(Structure.Layer[1].vSurfaceNormals);
    SetLength(ReflectedVecs,Num+1);
    for i:=0 to Num do begin
        ReflectedVecs[i]:=RotateVector(Structure.Layer[1].vSurfaceNormals[i],GetRayDir(1,i),Pi);
    end;
    //ShowMessage(IntToStr(Num));
  end;

  function TXRayTopo.GetPhotoImage(vNormal: Vector; D: extended; var Photo: TBitmap): integer;
  var
    Num,i: integer;
    P1,P2: Vector; //����� ������, �������� ����������� ��������������� ��������������� �����.
    PP: Vector; //������� ����� ����������� ������ P1,P2 � ���������� �������������
    t1: extended; //���������� �������� ��� ���������� ����� ����������� ������ � ����������
    MinX,MaxX,MinY,MaxY,MinZ,MaxZ: extended; //������� ���������� �������������
    //Photo: TBitmap;
    Val: Extended;
    Exp1,Exp01,Bright,Fon,Intens: integer;
    pixColor: TColor;
  begin
    Num:=High(ReflectedVecs);
    SetLength(PhotoFilm,Num+1);
    MinX:=MaxExtended; MinY:=MaxExtended; MinZ:=MaxExtended;
    MaxX:=MinExtended; MaxY:=MinExtended; MaxZ:=MinExtended; 
    for i:=0 to Num do begin
        P1:=GetVector(Structure.Layer[1].Mesh[i][1],Structure.Layer[1].Mesh[i][2],Structure.Layer[1].Mesh[i][3]);
        P2:=SumVecs(ReflectedVecs[i],P1);
        { TODO : ��������� ��������������� ���� �� ���������� ����� ������������� }
        t1:=-(vNormal[1]*P1[1]+vNormal[2]*P1[2]+vNormal[3]*P1[3]+D)/(vNormal[1]*(P2[1]-P1[1])+vNormal[2]*(P2[2]-P1[2])+vNormal[3]*(P2[3]-P1[3]));
        PhotoFilm[i][1]:=(P1[1]+(P2[1]-P1[1])*t1)*10;
        PhotoFilm[i][2]:=(P1[2]+(P2[2]-P1[2])*t1)*10;
        PhotoFilm[i][3]:=(P1[3]+(P2[3]-P1[3])*t1)*10;
        PhotoFilm[i][4]:=Intensity[i];

        //����������� ������ �������������
        if PhotoFilm[i][1] < MinX then MinX:=PhotoFilm[i][1];
        if PhotoFilm[i][1] > MaxX then MaxX:=PhotoFilm[i][1];
        if PhotoFilm[i][2] < MinY then MinY:=PhotoFilm[i][2];
        if PhotoFilm[i][2] > MaxY then MaxY:=PhotoFilm[i][2];
        if PhotoFilm[i][3] < MinZ then MinZ:=PhotoFilm[i][3];
        if PhotoFilm[i][3] > MaxZ then MaxZ:=PhotoFilm[i][3];
        //
    end;
    ShowMessage(FloatToStr(MinX)+'   '+FloatToStr(MaxX)+'   '+FloatToStr(MinY)+'   '+FloatToStr(MaxY));
    //Photo:=TBitmap.Create;
    Photo.Width:=Round(MaxX-MinX+1);
    Photo.Height:=Round(MaxY-MinY+1);
    Exp1:=8;
    Exp01:=8;
    Bright:=25;
    Fon:=0;
    for i:=0 to Num do begin
      Val:=(log10((PhotoFilm[i][4]+Random(Fon)*0.01)*power(10,Exp1+Exp01*0.1))*Bright);
      if (val<0) then Val:=0
      else If (Val>255) then Val:=255;
      Intens:=Round(Val);
      pixColor:=$00000001*Intens+$00000100*Intens+$00010000*Intens;
      Photo.Canvas.Pixels[Round(PhotoFilm[i][1]-MinX),Round(PhotoFilm[i][2]-MinY)]:=pixColor;
    end;
    SetLength(PhotoFilm,0);
  end;

end.
