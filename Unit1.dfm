object Form1: TForm1
  Left = 263
  Top = 50
  Width = 457
  Height = 618
  Caption = 'Topogram Modeling'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyUp = FormKeyUp
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Image1: TImage
    Left = 207
    Top = 0
    Width = 234
    Height = 560
    Align = alClient
    IncrementalDisplay = True
    Stretch = True
    OnMouseMove = Image1MouseMove
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 207
    Height = 560
    Align = alLeft
    TabOrder = 0
    object Label1: TLabel
      Left = 16
      Top = 520
      Width = 32
      Height = 13
      Caption = 'Label1'
      Visible = False
    end
    object Label15: TLabel
      Left = 16
      Top = 72
      Width = 69
      Height = 13
      Caption = 'Reflex indexes'
    end
    object Button1: TButton
      Left = 16
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Calc'
      Default = True
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 16
      Top = 36
      Width = 75
      Height = 25
      Caption = 'Save picture'
      TabOrder = 1
      OnClick = Button2Click
    end
    object SpinEdit1: TSpinEdit
      Left = 176
      Top = 41
      Width = 57
      Height = 22
      Hint = '????? ???????????? ????, ???.'
      MaxValue = 0
      MinValue = 0
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Value = 0
      Visible = False
    end
    object GroupBox1: TGroupBox
      Left = 8
      Top = 361
      Width = 97
      Height = 105
      Caption = ' Exposure '
      TabOrder = 3
      object SpinEdit2: TSpinEdit
        Left = 8
        Top = 19
        Width = 41
        Height = 22
        Hint = 'Contrast'
        MaxValue = 0
        MinValue = 0
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Value = 8
      end
      object SpinEdit3: TSpinEdit
        Left = 8
        Top = 51
        Width = 41
        Height = 22
        Hint = 'Brightnes'
        MaxValue = 0
        MinValue = 0
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Value = 25
      end
      object SpinEdit4: TSpinEdit
        Left = 48
        Top = 19
        Width = 41
        Height = 22
        Hint = 'Contrast (fraction)'
        MaxValue = 9
        MinValue = 0
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        Value = 8
      end
      object SpinEdit5: TSpinEdit
        Left = 8
        Top = 77
        Width = 41
        Height = 22
        Hint = 'Background'
        MaxValue = 0
        MinValue = 0
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        Value = 0
      end
    end
    object Button3: TButton
      Left = 8
      Top = 473
      Width = 97
      Height = 25
      Caption = 'Invert'
      TabOrder = 4
      OnClick = Button3Click
    end
    object GroupBox2: TGroupBox
      Left = 8
      Top = 154
      Width = 193
      Height = 67
      Caption = ' Buried layer thickness, A'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 5
      object Label5: TLabel
        Left = 105
        Top = 19
        Width = 16
        Height = 13
        Caption = 'To:'
      end
      object Label6: TLabel
        Left = 4
        Top = 19
        Width = 26
        Height = 13
        Caption = 'From:'
      end
      object Label7: TLabel
        Left = 8
        Top = 33
        Width = 10
        Height = 27
        Caption = 'e'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Symbol'
        Font.Style = []
        ParentFont = False
      end
      object Label8: TLabel
        Left = 16
        Top = 48
        Width = 7
        Height = 13
        Caption = '^'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Symbol'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TLabel
        Left = 23
        Top = 43
        Width = 3
        Height = 13
        Caption = ':'
      end
      object Edit4: TEdit
        Left = 32
        Top = 15
        Width = 65
        Height = 21
        Hint = 'Thicknes of buried layer on top side of picture (angstroms)'
        TabOrder = 0
        Text = '0'
      end
      object Edit5: TEdit
        Left = 123
        Top = 15
        Width = 65
        Height = 21
        Hint = 'Thicknes of buried layer on bottom side of picture (angstroms)'
        TabOrder = 1
        Text = '900'
      end
      object Edit6: TEdit
        Left = 32
        Top = 41
        Width = 65
        Height = 21
        Hint = 'Strain'
        TabOrder = 2
        Text = '0.0069'
      end
      object Edit10: TEdit
        Left = 120
        Top = 40
        Width = 65
        Height = 21
        TabOrder = 3
        Text = '0'
      end
    end
    object Button4: TButton
      Left = 100
      Top = 38
      Width = 75
      Height = 25
      Caption = 'Series'
      TabOrder = 6
      Visible = False
      OnClick = Button4Click
    end
    object GroupBox3: TGroupBox
      Left = 8
      Top = 229
      Width = 193
      Height = 71
      Caption = ' Angles '
      TabOrder = 7
      object Label10: TLabel
        Left = 16
        Top = 20
        Width = 15
        Height = 13
        Caption = 'Az:'
      end
      object Label11: TLabel
        Left = 101
        Top = 16
        Width = 21
        Height = 19
        Caption = 'Dq:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Symbol'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 6
        Top = 48
        Width = 25
        Height = 13
        Caption = 'Diap:'
      end
      object Label13: TLabel
        Left = 109
        Top = 40
        Width = 8
        Height = 23
        Caption = 'g'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Symbol'
        Font.Style = []
        ParentFont = False
      end
      object Label14: TLabel
        Left = 118
        Top = 48
        Width = 3
        Height = 13
        Caption = ':'
      end
      object Edit3: TEdit
        Left = 32
        Top = 16
        Width = 65
        Height = 21
        Hint = 'Azimuth, degrees'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Text = '0'
      end
      object Edit1: TEdit
        Left = 123
        Top = 16
        Width = 65
        Height = 21
        Hint = 'dTeta, arc.sec.'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Text = '0'
      end
      object Edit2: TEdit
        Left = 32
        Top = 44
        Width = 65
        Height = 21
        Hint = 'Beam divergence, arc.sec.'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        Text = '100'
      end
      object Edit9: TEdit
        Left = 123
        Top = 44
        Width = 65
        Height = 21
        Hint = 'dGamma, arc.sec.'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        Text = '0'
      end
    end
    object GroupBox4: TGroupBox
      Left = 8
      Top = 101
      Width = 193
      Height = 42
      Caption = ' Film thickness, A'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 8
      object Label2: TLabel
        Left = 4
        Top = 20
        Width = 26
        Height = 13
        Caption = 'From:'
      end
      object Label3: TLabel
        Left = 105
        Top = 20
        Width = 16
        Height = 13
        Caption = 'To:'
      end
      object Label4: TLabel
        Left = 64
        Top = 24
        Width = 32
        Height = 13
        Caption = 'Label4'
      end
      object Edit7: TEdit
        Left = 32
        Top = 16
        Width = 65
        Height = 21
        Hint = 'Film thicknes in angstroms (top side)'
        TabOrder = 0
        Text = '10000'
      end
      object Edit8: TEdit
        Left = 123
        Top = 16
        Width = 65
        Height = 21
        Hint = 'Film thicknes in angstroms (bottom side)'
        TabOrder = 1
        Text = '10000'
      end
    end
    object SpinEdit6: TSpinEdit
      Left = 8
      Top = 501
      Width = 41
      Height = 22
      Hint = 'Background'
      MaxValue = 100
      MinValue = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 9
      Value = 1
      OnChange = SpinEdit6Change
    end
    object ButtonImport: TButton
      Left = 8
      Top = 536
      Width = 97
      Height = 25
      Caption = 'Import'
      TabOrder = 10
      OnClick = ButtonImportClick
    end
    object CheckBox1: TCheckBox
      Left = 104
      Top = 15
      Width = 97
      Height = 17
      Caption = '+ rocking curve'
      TabOrder = 11
    end
    object SpinEdit7: TSpinEdit
      Left = 90
      Top = 70
      Width = 31
      Height = 22
      Hint = 'Reflex indexes'
      MaxValue = 0
      MinValue = 0
      TabOrder = 12
      Value = 0
    end
    object SpinEdit8: TSpinEdit
      Left = 120
      Top = 70
      Width = 31
      Height = 22
      Hint = 'Reflex indexes'
      MaxValue = 0
      MinValue = 0
      TabOrder = 13
      Value = 0
    end
    object SpinEdit9: TSpinEdit
      Left = 150
      Top = 70
      Width = 31
      Height = 22
      Hint = 'Reflex indexes'
      MaxValue = 0
      MinValue = 0
      TabOrder = 14
      Value = 4
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 560
    Width = 441
    Height = 19
    Panels = <
      item
        Width = 150
      end
      item
        Width = 150
      end
      item
        Width = 50
      end>
  end
  object SaveDialog1: TSaveDialog
    Filter = 'JPEG file (*.jpg)|*.jpg'
    Title = 'Save topogram as JPEG'
    Left = 72
    Top = 432
  end
  object OpenDialog1: TOpenDialog
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofEnableSizing]
    Left = 64
    Top = 504
  end
end
