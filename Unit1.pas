unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Grids, math, ComCtrls, JPEG,XRayMath, StrUtils,
  structure, vectors, mxarrays, Spin;

type
  TForm1 = class(TForm)
    Panel1: TPanel;
    Image1: TImage;
    Button1: TButton;
    Button2: TButton;
    Label1: TLabel;
    SpinEdit1: TSpinEdit;
    StatusBar1: TStatusBar;
    GroupBox1: TGroupBox;
    SpinEdit2: TSpinEdit;
    SpinEdit3: TSpinEdit;
    SpinEdit4: TSpinEdit;
    SaveDialog1: TSaveDialog;
    Button3: TButton;
    SpinEdit5: TSpinEdit;
    GroupBox2: TGroupBox;
    Edit4: TEdit;
    Edit5: TEdit;
    Button4: TButton;
    GroupBox3: TGroupBox;
    Edit3: TEdit;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit6: TEdit;
    GroupBox4: TGroupBox;
    Edit7: TEdit;
    Edit8: TEdit;
    SpinEdit6: TSpinEdit;
    ButtonImport: TButton;
    OpenDialog1: TOpenDialog;
    CheckBox1: TCheckBox;
    SpinEdit7: TSpinEdit;
    SpinEdit8: TSpinEdit;
    SpinEdit9: TSpinEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Edit9: TEdit;
    Edit10: TEdit;
    Label15: TLabel;

    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpinEdit1Change(Sender: TObject);
    procedure Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure SpinEdit6Change(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure ButtonImportClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

procedure GetTopFromDat(DatFileName: string; var x,y: extended);
function SortDatFiles(var DatFiles: TStrings; var L: array of extended): byte;
procedure DrawTopogram(ModDatFiles: TStrings; L: array of extended);
procedure ImportData(DatFilesNames: TStrings);

var
  Form1: TForm1;
  Topo: TXRayTopo;
implementation

{$R *.DFM}


procedure calc(Sender: TObject; H_start,H_end: extended);
var
  i,j,Intens,t0,w0,XRes,YRes,Half_YRes,Exp1,Exp01,Fon,Bright,n: integer;
  pixColor: TColor;
  Val,teta,y,dhkl,D,A,Rp,F,E,T,T1,T2,T_range,ta,eps,psi0,psi4,yy,lambda,
  tetaB,tetaWidth,tetaShift,SubstTetaSift,X1,Y1,Azimut_angle,H_range,R,beta: extended;
  LogFile: TextFile;
  StructFacts: string;
  SF: DynArray;
  Temp: TBitmap;
  //
  maxVal,minVal: extended;
  V: DynArray;
begin
  //maxVal:=0; minVal:=MaxExtended;
  //Topo.XRay.SetLambda(taCuKalfa1);
  //Topo.XRay.SetSourceParams(100000,0,48);
  Topo.SetLayerParams(1,5.4309,0,GetVector(-1,1,0));
  Topo.SetLayerParams(2,5.557,StrToF(Form1.Edit6.Text),GetVector(-1,1,0));
  Topo.SetLayerParams(3,5.4309,0,GetVector(-1,1,0));
  beta:=abs(StrToFloat(Form1.Edit2.Text));
  if beta=0 then R:=0 else
  R:=Topo.Structure.SizeX/2/cos(Pi/2-beta*Pi/3600/180);
  //R:=51700/6;
  Topo.SetCylLayerProfil(1,R,1000000,30,20,0*Pi/180);
  Topo.SetCylLayerProfil(2,R,1,30,20,0*Pi/180);
  Topo.SetCylLayerProfil(3,R,50000,30,20,0*Pi/180);
  //ShowMessage(FloatToStr(R));
  Topo.SetThicknessGradientY(3,StrToFloat(Form1.Edit7.Text),StrToFloat(Form1.Edit8.Text));
  Topo.SetThicknessGradientY(2,StrToFloat(Form1.Edit4.Text),StrToFloat(Form1.Edit5.Text));
  //Topo.SetLayerMisorientation(3,StrToFloat(Form1.Edit10.Text)/3600/180*Pi,-10,0,'y');
  //if Form1.CheckBox2.Checked then
  //Topo.SetLWinToStruct(7);
  //Topo.SetCrystalOrientation(45*Pi/180);
   Topo.SetStructureOrientation(Form1.SpinEdit7.Value,Form1.SpinEdit8.Value,Form1.SpinEdit9.Value,StrToFloat(Form1.Edit3.Text)*Pi/180,StrToFloat(Form1.Edit9.Text)*Pi/180,StrToFloat(Form1.Edit1.Text)*Pi/3600/180);
  //MyStruct.Layer[1].ExportReliefMatrix('layer1.csv');
//  Image1.Visible:=False;
  {AssignFile(LogFile,'structfact.txt');
  Reset(LogFile);
  ReadLn(LogFile,StructFacts);
  SF:=mxExplode(';',StructFacts);
  maxVal:=StrToFloat(SF[5]);
  ShowMessage(FloatToStr(maxVal));
  CloseFile(LogFile); }

  with Form1 do begin

  //Image1.Picture.Create;
  //Image1.Picture.Bitmap.Width:=Image1.width;
  //Image1.Picture.Bitmap.Height:=Image1.Height;

  Exp1:=SpinEdit2.value;
  Exp01:=SpinEdit4.value;
  Bright:=SpinEdit3.value;
  Fon:=SpinEdit5.value;

  XRes:=Round(Topo.Structure.SizeX*Topo.Structure.Layer[1].DPmmX);
  YRes:=Round(Topo.Structure.SizeY*Topo.Structure.Layer[1].DPmmY);

  Temp:=TBitmap.Create;
  Temp.Width:=XRes;
  Temp.Height:=YRes;

  //Topo.GetIntensity()
  Topo.GetIntensityTT2();
  //Topo.GetReflectedVectors();
  //Topo.GetPhotoImage(GetVector(0,0,1),2,Temp);

  if Form1.CheckBox1.Checked then
  begin
     Topo.GetKDO(1);
  end;


  n:=Round(Topo.Structure.SizeX*Topo.Structure.Layer[1].DPmmX);

  for i:=0 to Round(Topo.Structure.SizeY*Topo.Structure.Layer[1].DPmmY)-1 do begin
    for j:=0 to Round(Topo.Structure.SizeX*Topo.Structure.Layer[1].DPmmX)-1 do begin
     //Val:=Rp*(3+2*cos(F)*(cos(D)-Cos(E))+2*sin(F)*(Sin(D)-Sin(E))-2*cos(D-E));
     Val:=Topo.Intensity[i*n+j];
     Val:=(log10((Val+Random(Fon)*0.01)*power(10,Exp1+Exp01*0.1))*Bright);
     //Val:=(Topo.FallAngles[i*100+j]-Topo.Teta0)*180/Pi*3600*15937.5+127;//*16262-562041;
     //if Val>maxVal then maxVal:=Val;
     //if Val<minVal then minVal:=Val;
     //if Val>34.5 then showmessage('ok');
     if (val<0) then Val:=0
     else If (Val>255) then Val:=255;
     Intens:=Round(Val);
     pixColor:=$00000001*Intens+$00000100*Intens+$00010000*Intens;
     Temp.Canvas.Pixels[j,i]:=pixColor;
    end;
  end;
  end;

//  Form1.Image1.Canvas.StretchDraw(Form1.Image1.ClientRect,Temp);
  Form1.Image1.Picture.Bitmap.Assign(Temp);
end;


procedure TForm1.Button1Click(Sender: TObject);
begin
  calc(Sender,StrToFloat(Edit4.Text),StrToFloat(Edit5.Text));
end;

procedure SaveJpeg(Pic: TBitmap; FileName: string);
var
  MyJpeg: TJpegImage;
begin
  if RightStr(FileName, 4)<>'.jpg' then FileName:=FileName+'.jpg';
  MyJpeg:= TJpegImage.Create;
  MyJpeg.Assign(Pic); // ���������� ����������� ������� MyJpeg
  MyJpeg.SaveToFile(FileName); // ���������� �� ����� ����������� � ������� JPEG
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  if SaveDialog1.Execute then begin
    SaveJpeg(Image1.Picture.Bitmap, SaveDialog1.FileName); // ���������� �� ����� ����������� � ������� JPEG
  end;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
//  Form1.Button1Click(Sender);
  Topo:=TXRayTopo.Create;
  Topo.AddStructure(10,20,3);
  Topo.AddXRay;
 Randomize;
end;

procedure TForm1.SpinEdit1Change(Sender: TObject);
begin
  Form1.Button1Click(Sender);
end;

procedure TForm1.Image1MouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  Label1.Caption:='X='+IntToStr(X)+'   Y='+IntToStr(y);
end;

procedure TForm1.Button3Click(Sender: TObject);
var
  i,j,x,y: integer;
  Temp: TBitmap;
begin
  Temp:=TBitmap.Create;
  //Temp.Width:=Image1.Width;
  //Temp.Height:=Image1.Height;
  Temp:=Image1.Picture.Bitmap;
  x:=Image1.Width;
  y:=Image1.Height;


  for i:=0 to x-1 do
    for j:=0 to  y-1 do begin
      with Temp.Canvas do begin
      Pixels[i,j]:=$00FFFFFF xor Pixels[i,j];
      Pixels[i,j]:=$00FFFFFF and Pixels[i,j];
      end;

    end;
  Image1.Picture.Bitmap:=Temp;
end;

procedure TForm1.Button4Click(Sender: TObject);
var
  i,NFrames: integer;
  HStt,HEnd,HSt0,HSt1,GradH,StepH: extended;
  FName: string;
begin
  HSt0:=0;
  Hst1:=20000;
  StepH:=100;
  GradH:=900;

  NFrames:=Trunc((Hst1-HSt0)/100);

  for i:=0 to NFrames do begin

    HStt:=i*StepH+HSt0;
    HEnd:=HStt+GradH;
    calc(Sender,HStt,HEnd);

    FName:='D:\Anton\Programs\Diffraction_Milita\Film\'+FormatFloat('0000',i)+
           ' ������� '+FormatFloat('000000',HStt)+'-'+FormatFloat('000000',HEnd)+'.jpg';
    SaveJpeg(Image1.Picture.Bitmap, FName);
  end;


end;

procedure TForm1.SpinEdit6Change(Sender: TObject);
var
  eps,lambda,tetaB,tetaBf,w,Ung,aSub,aBur: extended;

begin
  eps:=StrToF(Edit6.Text);
  aSub:=5.428;
  lambda:=1.540562E-10;
  tetaB:=BraggRad(aSub,lambda*1E10,0,0,4);

  aBur:=eps*aSub+aSub;
  tetaBf:=BraggRad(aBur,lambda*1E10,0,0,4);

  w:=tetaBf-tetaB;


  Ung:=arctan( ((-2*w*w*cos(tetaBf))/(SpinEdit6.Value*lambda))* 1e-10 *180*3600/PI     )*180/PI;

  StatusBar1.Panels[0].Text:=FloatToStr(90+Ung);

end;

procedure TForm1.FormResize(Sender: TObject);
begin
  StatusBar1.Panels[1].Text:=IntToStr(Image1.Width)+' x '+IntToStr(Image1.Height);
end;

function SortDatFiles(var DatFiles: TStrings; var L: array of extended): byte;
var
  Len,i,_pos,n: integer;
  //L: array of extended;
  tmpL: extended;
begin
  //SetLength(L,DatFiles.Count);
    if DatFiles.Count<3 then begin result:=1; exit; end else result:=0;
    _pos:=1;
    //ShowMessage(DatFiles.Strings[2][L]);
    for n:=0 to DatFiles.Count-1 do
    begin
      Len:=Length(DatFiles.Strings[2]);
      for i:=Len-4 downto 1 do
        if DatFiles.Strings[n][i]='_' then
        begin
          _pos:=i;
          break;
      end;
      L[n]:=StrToFloat(DatFiles.Strings[n][_pos+1]+DatFiles.Strings[n][_pos+2]+DecimalSeparator+DatFiles.Strings[n][_pos+4]+DatFiles.Strings[n][_pos+5]);
    end;
    for n:=0 to DatFiles.Count-2 do
      for i:=n+1 to DatFiles.Count-1 do
        if L[n]>L[i] then begin DatFiles.Exchange(n,i); tmpL:=L[n]; L[n]:=L[i]; L[i]:=tmpL; end;
    //L:=nil;
end;

procedure GetTopFromDat(DatFileName: string; var x,y: extended);
var
  SL: TStrings;
 // XY: array of array [0..1] of extended;
  DatFile: TextFile;
  TopMaxY: extended;
  i,j,k: integer;
  s,sY,sX: string;
begin
  SL:=TStringList.Create;
  AssignFile(DatFile,DatFileName);
  Reset(DatFile);
  try
    SL.LoadFromFile(DatFileName);
    //NL:=SL.Count;
   // SetLength(XY,SL.Count);
    y:=-1;
    for i:=0 to SL.Count-1 do
    begin
      ReadLn(DatFile,s);
      for j:=1 to Length(s) do
      begin
        if s[j]=',' then
        begin
          for k:=j+1 to Length(s) do sY:=sY+s[k];
          break;
        end;
        if (s[j]='.')and(DecimalSeparator<>'.') then sX:=sX+','
        else sX:=sX+s[j];
      end;
      TopMaxY:=StrToFloat(sY);
      if y<TopMaxY then
      begin
        y:=TopMaxY;
        x:=StrToFloat(sX);
      end;
      sY:=''; sX:='';
    end;
  finally
    SL.Free;
    CloseFile(DatFile);
  end;
end;

function ReplaceStr(s,s_find,s_replace:string):string;
begin
    if pos(s_find,s)>=0 then
        result:=copy(s,1,pos(s_find,s)-1)+s_replace+copy(s,pos(s_find,s)+
                    length(s_find),length(s)-(pos(s_find,s)+length(s_find))+1)
    else
        result:=s;
end;

procedure GetXYfromStr(str: string; var x,y: extended);
var i: integer;
    sx,sy: string;
begin
  sx:=''; sy:='';
  for i:=1 to Length(str) do
  begin
    if str[i]=',' then begin sy:=copy(str,i+1,Length(str)); break; end;
    sx:=sx+str[i];
  end;
  x:=StrToF(sx);
  y:=StrToF(sy);
end;


procedure DrawTopogram(ModDatFiles: TStrings; L: array of extended);
var
  bmTopogram: TBitmap;
  XY: TStrings;
  i,j,k,Nsteps,bmHeight,Intens,Bright,Exp1,Exp01: integer;
  x,y: extended;
  pixColor: TColor;
begin
  bmTopogram:=TBitmap.Create;
  bmTopogram.Width:=1000;
  //bmTopogram.Height:=Form1.Image1.Height;
  Bright:=Form1.SpinEdit3.value;
  Exp1:=Form1.SpinEdit2.value;
  Exp01:=Form1.SpinEdit4.value;
  bmHeight:=0;
  for i:=0 to ModDatFiles.Count-1 do
  begin
   if i<ModDatFiles.Count-1 then
   begin
     Nsteps:=Round((L[i+1]-L[i])*10)*1;
     Inc(bmHeight,Nsteps);
   end;
  end;
  Inc(bmHeight,1);
  bmTopogram.Height:=bmHeight*2;
  bmHeight:=1;
  XY:=TStringList.Create;
  try
    for i:=0 to ModDatFiles.Count-1 do
    begin
      XY.LoadFromFile(ModDatFiles[i]);
      if i<ModDatFiles.Count-1 then Nsteps:=Round((L[i+1]-L[i])*10)*1 else Nsteps:=1;
      for k:=1 to Nsteps do
      begin
        for j:=0 to XY.Count-1 do
        begin
          GetXYfromStr(XY[j],x,y);
          if y=0 then begin y:=1; end;
          //y:=(log10((y*0.001)*power(10,Exp1+Exp01*0.1))*Bright);
          y:=log10(y*1)*2*Bright;
          Intens:=Round(y);
          x:=x/0.3;
          if Intens>255 then Intens:=255 else if Intens<0 then Intens:=0;
          pixColor:=$00000001*Intens+$00000100*Intens+$00010000*Intens;
          bmTopogram.Canvas.Pixels[Round(x),bmHeight]:=pixColor;
        end;
        Inc(bmHeight);
      end;
    end;
    //bmTopogram.Canvas.Ellipse(0,0,50,50);
    //Form1.Image1.Canvas.StretchDraw(Form1.Image1.ClientRect,bmTopogram);
    Form1.Image1.Picture.Bitmap.Assign(bmTopogram);
  finally
    bmTopogram.Free;
    XY.Free;
  end;
end;


procedure ImportData(DatFilesNames: TStrings);
var
  X,Y,MaxX,MaxY: extended;
  i,j,k: integer;
  FtoEdit,NewFiles: TStrings;
  sX,sXnew,Folder,CurTime: string;
  L: array of extended;
//Temp

begin
  SetLength(L,DatFilesNames.Count);
  SortDatFiles(DatFilesNames,L);
  CurTime:=TimeToStr(Time);
  Folder:=ExtractFileDir(Application.ExeName)+'\'+DateToStr(Date)+' - '+CurTime[1]+CurTime[2]+'.'+CurTime[4]+CurTime[5]+'.'+CurTime[7]+CurTime[8];
  CreateDir(Folder);
  NewFiles:=TStringList.Create;
  FtoEdit:=TStringList.Create;
  try
    NewFiles.Add(Folder+'\mod '+ExtractFileName(DatFilesNames[0]));
    GetTopFromDat(DatFilesNames[0],MaxX,MaxY);
    FtoEdit.LoadFromFile(DatFilesNames.Strings[0]);
    FtoEdit.SaveToFile(Folder+'\mod '+ExtractFileName(DatFilesNames[0]));
    FtoEdit.Clear;
    for i:=1 to DatFilesNames.Count-1 do
    begin
      GetTopFromDat(DatFilesNames[i],X,Y);
      FtoEdit.LoadFromFile(DatFilesNames.Strings[i]);
      for j:=0 to FtoEdit.Count-1 do
      begin
        sX:='';
        for k:=1 to Length(FtoEdit[j]) do
        begin
          //if FtoEdit[j][k]='.' then begin sX:=sX+DecimalSeparator; ppos:=k; continue; end;
          if FtoEdit[j][k]<>',' then sX:=sX+FtoEdit[j][k] else break;
        end;
        sXnew:=FloatToStr(StrToF(sX)-(X-MaxX));
        for k:=1 to Length(sXnew) do if sXnew[k]=',' then begin sXnew[k]:='.'; break; end; 
        FtoEdit.Strings[j]:=ReplaceStr(FtoEdit[j],sX,sXnew);
      end;
      FtoEdit.SaveToFile(Folder+'\mod '+ExtractFileName(DatFilesNames[i]));
      NewFiles.Add(Folder+'\mod '+ExtractFileName(DatFilesNames[i]));
      FtoEdit.Clear;
    end;
    DrawTopogram(NewFiles,L);
  finally
    FtoEdit.Free;
    NewFiles.Free;
    L:=nil;
  end;
end;


procedure TForm1.ButtonImportClick(Sender: TObject);
begin
  if OpenDialog1.Execute then ImportData(OpenDialog1.Files);
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  Topo.Free;
end;

procedure TForm1.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=13 then begin
    if ActiveControl is TEdit then TEdit(ActiveControl).SelectAll
    else if ActiveControl is TSpinEdit then begin
      Button1Click(Sender);
      TSpinEdit(ActiveControl).SelectAll;
    end;
  end;
end;

end.
