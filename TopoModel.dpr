program TopoModel;

{%ToDo 'TopoModel.todo'}

uses
  Forms,
  Unit1 in 'Unit1.pas' {Form1},
  XRayMath in 'XRayMath.pas',
  CompMath in 'CompMath.pas',
  structure in 'structure.pas',
  vectors in 'vectors.pas',
  mxarrays in 'mxarrays.pas',
  mxcommon in '..\..\MXLib\mxcommon.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
